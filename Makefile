
all: server client rom package

# external shit.  Link your own
#	https://www.3dbrew.org/wiki/Makerom
MAKEROM 	= ../../tools/bin/makerom
SCRIPTDIR	= scripts

# configuration
# icon and banner
#	https://www.3dbrew.org/wiki/CiTRUS
#	https://github.com/polaris-/ctpktool
ROM_DIR		= bin/rom
ROM_OUTPUT 	= $(ROM_DIR)/3DSRemoteDesktopClient.cia
ROM_SPEC	= 3DSRemoteDesktopClient/app.rsf
ROM_ELF		= bin/client/3DSRemoteDesktopClient.elf
ROM_ICON 	= 3DSRemoteDesktopClient/3dsRemoteDesktop.xbsf.icn
ROM_BANNER	= 3DSRemoteDesktopClient/3dsRemoteDesktop.xbsf.bnr
ROM_DESC	= app:4

# depends on ncftp
DEPENDENCYSCRIPT	= ./dependencies.sh
PACKAGESCRIPT		= ./package.sh
SENDSCRIPT			= ./deploy.sh

.PHONY: server client rom clean deploy dependencies

server:
	@(cd 3DSRemoteDesktopServer; make -s)

client:
	@(cd 3DSRemoteDesktopClient; make -s)

rom: client
	@($(MAKEROM) -f cia -o $(ROM_OUTPUT) -rsf $(ROM_SPEC) -target t -elf $(ROM_ELF) -icon $(ROM_ICON) -banner $(ROM_BANNER) -desc $(ROM_DESC) -exefslogo -v )

deploy: rom
	@(cd $(SCRIPTDIR); $(SENDSCRIPT))

dependencies:
	@(cd $(SCRIPTDIR); $(DEPENDENCYSCRIPT))

package: server client rom
	@(cd $(SCRIPTDIR); $(PACKAGESCRIPT))

clean:
	@(cd 3DSRemoteDesktopClient; make -s clean)
	@(cd 3DSRemoteDesktopServer; make -s clean)
	@(rm -d -r bin)
	@(echo builds clean...)
	@(rm *.7z)
	@(echo packages clean...)
	@(echo done)

$(shell mkdir -p $(ROM_DIR))