
#include "Packet.h"
#include "Types.h"

#ifndef _RETRONETWORK_H_
#define _RETRONETWORK_H_

#ifdef CTR
#include <3ds.h>
#endif // CTR

#if defined(LINUX) || defined(CTR)

#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <signal.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <netdb.h>

#if !defined(CTR)
#include <ifaddrs.h>
#endif // !CTR

static int32 _get_errno_hack_() { return errno; }

#define RNioctl ioctl
#define RNCloseSocket close

#define RNEWOULDBLOCK EWOULDBLOCK
#define RNCONNRESET ECONNRESET
#define RNBADSOCKET -1
#define RNSOCKETERROR -1
#define GETERRNO _get_errno_hack_
typedef int32 RNSocket;
#endif // LINUX || CTR


#ifdef WIN32
#include <winsock2.h>
#include <ws2tcpip.h>
//#include <windows.h>

#define RNioctl ioctlsocket
#define RNCloseSocket closesocket

#define RNEWOULDBLOCK WSAEWOULDBLOCK
#define RNCONNRESET WSAECONNRESET
#define RNBADSOCKET INVALID_SOCKET
#define RNSOCKETERROR SOCKET_ERROR
#define GETERRNO WSAGetLastError
typedef SOCKET RNSocket;

#endif // WIN32

#ifdef __cplusplus
extern "C" {
#endif

// in:
//      Socket to send to
//      packet ID of packet
//      packet data size
//      packet data
// ret:
//      -1 if send() error
//      else size of data sent(nDataSize + sizeof(Packet))
int32 RNSendData(int32 DestinationSocket, uint32 nPacketID, uint32 nDataSize, void* Data);


// in:
//      in-the socket to recv from
//      out-pRecvPacket - the packet we are requesting
//      out-nGetError - socket error code
// ret:
//       0 if the socket was closed cleanly
//      -1 if socket error
//          nGetError will hold the socket error code
//      -2 if unable to allocate data for the packet
//          nGetError will hold the size of the data we tried to allocate
//      >0 is the packet size retrieved(NOT the size of the packet data)
// NOTE: Dont get the errno if this function fails, it will be returned
int32 RNRecieveData(int32 RecvSocket, Packet** pRecvPacket, int32* nGetErrno);


#ifdef __cplusplus
}
#endif


#endif // _RETRONETWORK_H_
