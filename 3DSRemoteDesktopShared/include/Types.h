#ifndef _TYPES_
#define _TYPES_

#if defined(CTR)
#include <3ds/types.h>
typedef s8      int8;
typedef s16     int16;
typedef s32     int32;
typedef s64     int64;
typedef u8      uint8;
typedef u16     uint16;
typedef u32     uint32;
typedef u64     uint64;

#else
typedef char                int8;
typedef short               int16;
typedef int                 int32;
typedef long                int64;
typedef long long           int128;
typedef unsigned char       uint8;
typedef unsigned short      uint16;
typedef unsigned int        uint32;
typedef unsigned long       uint64;
typedef unsigned long long  uint128;

#endif

typedef uint8 byte;

#if !defined(__cplusplus)
#include <stdbool.h>
#endif

#if defined(RELEASE)
#define TWEAKABLE const
#else
#define TWEAKABLE
#endif // RELEASE

#if defined(WIN32)
#elif defined(LINUX)
#elif defined(OSX)
#elif defined(CTR)
#endif // platform block

#endif // _TYPES_