
#ifndef _PACKET_H_
#define _PACKET_H_

#include "Types.h"

typedef enum _PacketType
{
    PACKET_INPUT_PRESSED,
    PACKET_INPUT_RELEASED,
    PACKET_SCREEN,
    PACKET_SCREENRETRIEVED,
    PACKET_HANDSHAKEREQUEST,
    PACKET_HANDSHAKERESPONSE,
    PACKET_MESSAGE,
    PACKET_INPUT_CIRCLEPAD,
    PACKET_TOTAL
} PacketType;

typedef struct
{
    uint32 _PacketID;
    uint32 _PacketSize;
    byte _PacketData[];
} Packet;

typedef struct 
{
    uint16 X;
    uint16 Y;
    uint16 Width;
    uint16 Height;
    byte   PixelData[];
} ScreenDescriptor;

typedef struct
{
  uint32 TotalSize; // size when decompressed
  uint32 CompressedSize; // size when compressed
  ScreenDescriptor Data;
} ScreenPacketData;

#endif // _PACKET_H_
