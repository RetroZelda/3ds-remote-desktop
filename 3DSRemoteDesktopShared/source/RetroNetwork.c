#include "RetroNetwork.h"

#ifdef CTR
#include "ExtendedConsole.h"
#include <string.h>
#endif // CTR

#ifdef LINUX
#include <stdlib.h>
#include <string.h>
#endif

int32 RNSendData(int32 DestinationSocket, uint32 nPacketID, uint32 nDataSize, void* Data)
{
    // create the packet to send
    int32 nPacketLength = sizeof(Packet) + (nDataSize);
    Packet* packet = (Packet*)malloc(nPacketLength);
    packet->_PacketID = nPacketID;
    packet->_PacketSize = nDataSize;
    memcpy(packet->_PacketData, Data, nDataSize);

    int32 nTotalSent = 0;
    int32 nSendAmmount = 0;
    do
    {
        nSendAmmount = (int32)send(DestinationSocket, (int8*)packet + nTotalSent, nPacketLength, 0);
        if(nSendAmmount < 0)
        {
            free(packet);
            return -1;
        }
        nTotalSent += nSendAmmount;
    } while(nTotalSent < nPacketLength);

    free(packet);

    return nTotalSent;
}

int32 RNRecieveData(int32 RecvSocket, Packet** pRecvPacket, int32* nGetErrno)
{
    int32 nPacketSize = sizeof(Packet);
    Packet tempPacket;
    int32 ret = recv(RecvSocket, (int8*)&tempPacket, nPacketSize, 0);
    if(ret > 0)
    {
        // determine hte full size of the data coming in
        int32 nTotalSize = tempPacket._PacketSize + nPacketSize;
        *pRecvPacket = (Packet*)malloc(nTotalSize);
        if(*pRecvPacket == NULL)
        {
            if(nGetErrno)
            {
                *nGetErrno = nTotalSize;
            }
            return -2;
        }

        // put the temp into the final
        memcpy(*pRecvPacket, (byte*)&tempPacket, nPacketSize);

        // recieve everything if we need to
        int32 nTotalRetrieved = nPacketSize;
        if(tempPacket._PacketSize > 0)
        {
            do
            {
                ret = recv(RecvSocket, (int8*)*pRecvPacket + nTotalRetrieved, nTotalSize - nTotalRetrieved, 0 );
                if(ret > 0)
                {
                    nTotalRetrieved += ret;
                }
                else
                {
                    // if not EWOULDBLOCK, leave while setting the error
                    uint32 error = GETERRNO();
                    #ifdef WIN32
                    if(error != WSAEWOULDBLOCK)
                    #else
                    if(-error != EWOULDBLOCK && -error != EAGAIN)
                    #endif // WIN32
                    {
                        if(nGetErrno)
                        {
                            free(*pRecvPacket);
                            *pRecvPacket = NULL;
                            *nGetErrno = error;
                            return -1;
                        }
                    }
                }
            }while(nTotalRetrieved < nTotalSize);
        }
        ret = nTotalRetrieved;
    }
    else
    {
        if(ret < 0)
        {
            if(nGetErrno)
            {
                *nGetErrno = GETERRNO();
                return -1;
            }
        }
        return -1;
    }
    return ret;
}
