
#include "ServerSettings.h"
#include <iostream>
#include <fstream>

CServerSettings::CServerSettings(void)
{
	// the 3ds screen size
	m_unDestinationWidth = 400;
	m_unDestinationHeight = 240;
}

CServerSettings::~CServerSettings(void)
{
}

bool CServerSettings::LoadSettings(std::string file_name)
{
    std::string option, equals, value;
    std::ifstream myfile (file_name.c_str());
    if (myfile.is_open())
    {
        while ( myfile >> option >> equals >> value )
        {
            int32 nTrueValue = atoi(value.c_str());
            if(option == "PORT")
            {
                m_sPort = (int16)nTrueValue;
            }
            else if(option == "CPDEADZONE")
            {
                m_sDeadzone = (int16)nTrueValue;
            }
            else if(option == "SCREEDWIDTH")
            {
                m_unDestinationWidth = nTrueValue;
            }
            else if(option == "SCREENHEIGHT")
            {
                m_unDestinationHeight = nTrueValue;
            }
            else if(option.find("BTN_", 0) != std::string::npos)
            {
                int32 nBtnValue = -1;
                if(option == "BTN_A")
                {
                    nBtnValue = CTR_KEY_A;
                }
                else if(option == "BTN_B")
                {
                    nBtnValue = CTR_KEY_B;
                }
                else if(option == "BTN_X")
                {
                    nBtnValue = CTR_KEY_X;
                }
                else if(option == "BTN_Y")
                {
                    nBtnValue = CTR_KEY_Y;
                }
                else if(option == "BTN_L")
                {
                    nBtnValue = CTR_KEY_L;
                }
                else if(option == "BTN_R")
                {
                    nBtnValue = CTR_KEY_R;
                }
                else if(option == "BTN_DUP")
                {
                    nBtnValue = CTR_KEY_DUP;
                }
                else if(option == "BTN_DDOWN")
                {
                    nBtnValue = CTR_KEY_DDOWN;
                }
                else if(option == "BTN_DLEFT")
                {
                    nBtnValue = CTR_KEY_DLEFT;
                }
                else if(option == "BTN_DRIGHT")
                {
                    nBtnValue = CTR_KEY_DRIGHT;
                }
                else if(option == "BTN_CPUP")
                {
                    nBtnValue = CTR_KEY_CPAD_UP;
                }
                else if(option == "BTN_CPDOWN")
                {
                    nBtnValue = CTR_KEY_CPAD_DOWN;
                }
                else if(option == "BTN_CPLEFT")
                {
                    nBtnValue = CTR_KEY_CPAD_LEFT;
                }
                else if(option == "BTN_CPRIGHT")
                {
                    nBtnValue = CTR_KEY_CPAD_RIGHT;
                }
                else
                {
                    std::cout << "Unhandled button [" << option << equals << value << "]\n";
                }
                m_KeyBinding[nBtnValue] = nTrueValue;
            }
            else
            {
                std::cout << "Unhandled setting [" << option << equals << value << "]\n";
            }
        }
        myfile.close();

        // the RECT that represents the destination's screen
        m_DestinationRect.left = 0;
        m_DestinationRect.right = m_unDestinationWidth;
        m_DestinationRect.top = 0;
        m_DestinationRect.bottom = m_unDestinationHeight;
    }

    else
    {
        std::cout << "\tUnable to open \"" << file_name << "\"" << std::endl;
    }

}







