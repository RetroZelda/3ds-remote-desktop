#include "linux/CaptureSCL.h"
#include "Global.h"

extern "C"
{
    #include <libswscale/swscale.h>
};

namespace
{
    static TWEAKABLE uint16 BUFFER_SIZE = 5;

    struct SwsContext* m_ScaleContext = nullptr;
}

CCaptureSCL::CCaptureSCL()
{
}

CCaptureSCL::~CCaptureSCL()
{
    assert(!m_ScaleContext && "CCaptureSCL didnt shutdown properly");
    sws_freeContext(m_ScaleContext);
}

void CCaptureSCL::Init(std::weak_ptr<IWindowData> window)
{
    assert(!m_CaptureConfiguration && "Capture already initialized");
    m_WindowData = window.lock();

    // allocate the space we will use to copy the screen data into
    m_ScreenCacheSize = m_WindowData->GetWidth() * m_WindowData->GetWidth() * sizeof(ImageBGRA);
    m_ScaleCacheSize = CGlobal::ServerSettings->GetDestinationWidth() * CGlobal::ServerSettings->GetDestinationHeight() * sizeof(ImageRGB565);
    m_ScreenCache = (byte*)aligned_alloc(sizeof(ImageBGRA), m_ScreenCacheSize);
    m_ScaleCache = (byte*)aligned_alloc(sizeof(ImageRGB565), m_ScaleCacheSize);

    // setup screen capture light
    m_CaptureConfiguration = SL::Screen_Capture::CreateCaptureConfiguration(std::bind(&CCaptureSCL::OnMonitorsCaptured, this));
    m_CaptureConfiguration->onFrameChanged([this](const ScreenImage &img, const ScreenMonitor &monitor)
    {
        this->OnFrameChanged(img, monitor);
    });
    m_CaptureConfiguration->onNewFrame([this](const ScreenImage &img, const ScreenMonitor &monitor)
    {
        this->OnNewFrame(img, monitor);
    });

    // setup swscale
    m_ScaleContext = sws_getContext(m_WindowData->GetWidth(), m_WindowData->GetHeight(), AV_PIX_FMT_BGRA, 
        CGlobal::ServerSettings->GetDestinationWidth(), CGlobal::ServerSettings->GetDestinationHeight(), AV_PIX_FMT_RGB565LE, 
        SWS_FAST_BILINEAR, NULL, NULL, NULL);
}

void CCaptureSCL::Shutdown()
{
    if(IsRunning())
        Stop();

    free(m_ScaleCache);
    free(m_ScreenCache);
    sws_freeContext(m_ScaleContext);
    m_CaptureConfiguration = nullptr;    
}

void CCaptureSCL::Start()
{
    assert(!IsRunning() && "Capture already running");
    if(IsRunning())
        return;
        
    m_CaptureManager = m_CaptureConfiguration->start_capturing();
}

void CCaptureSCL::Resume()
{
    m_CaptureManager->resume();
}

void CCaptureSCL::Pause()
{
    m_CaptureManager->pause();
}

void CCaptureSCL::Stop()
{
    m_CaptureManager = nullptr;

    // clear the buffer
    while(m_FrameBuffer.size() > 0)
    {
        CaptureData& front = m_FrameBuffer.front();
        free(front.m_Data);
        m_FrameBuffer.pop_front();
    }
}

uint32 CCaptureSCL::GetNextFrame(CaptureData& out_data)
{
    LOCK_MUTEX(m_BufferLock); 
    if(m_FrameBuffer.size() > 0)
    {
        out_data = m_FrameBuffer.front();
        m_FrameBuffer.pop_front();

        return out_data.m_Size;
    }
    return 0;
}

std::vector<ScreenMonitor> CCaptureSCL::OnMonitorsCaptured()
{
    std::vector<ScreenMonitor> moniter_list = SL::Screen_Capture::GetMonitors();
    // std::cout << "Library is requesting the list of monitors to capture!" << std::endl;
    return moniter_list;
}

void CCaptureSCL::OnFrameChanged(const ScreenImage &img, const ScreenMonitor &monitor)
{    
    AddFrame(img, monitor, EFrameType::CHANGE);
}

void CCaptureSCL::OnNewFrame(const ScreenImage &img, const ScreenMonitor &monitor)
{
    AddFrame(img, monitor, EFrameType::FULL);
}

void CCaptureSCL::GetFinalFrame(const ImageRGB565* source_buffer, CaptureData& captured_frame, int32 src_width, int32 src_height) const
{
    int32 width = captured_frame.m_Width;
    int32 height = captured_frame.m_Height;
    uint32 pixel_size = sizeof(ImageRGB565);

    // have to loop to rotate -90 degrees for the 3ds screen
    for(int32 y = 0; y < height; ++y)
    {
        int32 offset_y = captured_frame.m_Y + y;
        const ImageRGB565* pRow = source_buffer + (src_width * offset_y + captured_frame.m_X);
        for(int32 x = 0; x < width; ++x)
        {
            ImageRGB565* dest = ((ImageRGB565*)captured_frame.m_Data) + (height * x + (height - y - 1));
            *dest = *(pRow++);
        }
    }
}

/*
#define TJE_IMPLEMENTATION
#include "tiny_jpeg_RGB565_HACK.h"
*/
void CCaptureSCL::AddFrame(const ScreenImage &img, const ScreenMonitor &monitor, EFrameType frame_type)
{
    int32 img_width = Width(img);
    int32 img_height = Height(img);
    int32 frame_width = m_WindowData->GetWidth();
    int32 frame_height = m_WindowData->GetHeight();
    int32 final_width = CGlobal::ServerSettings->GetDestinationWidth();
    int32 final_height = CGlobal::ServerSettings->GetDestinationHeight();
    if(img_width != 0 && img_height != 0)
    {
        // extract the frame bounds
        const struct extractor { int32 left; int32 top; int32 right; int32 bottom; }& 
            img_rect = *(extractor*)&img;

        // extract the raw image data so we can convert it
        if (isDataContiguous(img) && frame_type == EFrameType::FULL)
        {
            // no padding, the entire copy can be a single memcpy call
            memcpy(m_ScreenCache, StartSrc(img), img_width * img_height * sizeof(ImageBGRA));
        }
        else 
        {
            // lets fill the buffer based on the rect
            const ImageBGRA* startsrc = StartSrc(img);
            for (auto i = img_rect.top; i < img_rect.top + img_height; i++) 
            {
                byte* startdst = m_ScreenCache + (((i * frame_width) + img_rect.left) * sizeof(ImageBGRA));
                memcpy(startdst, startsrc, sizeof(ImageBGRA) * img_width);
                startsrc = GotoNextRow(img, startsrc);
            }
        }
        
        // determine the scaled portion of the screen
        float width_ratio = ((float)img_width / (float)frame_width);// * final_width;
        float height_ratio = ((float)img_height / (float)frame_height);// * final_height;
        float width_scale = (float)final_width / (float)img_width;
        float height_scale = (float)final_height / (float)img_height;

        CaptureData new_capture;
        new_capture.m_X = (uint16)((((float)img_rect.left * width_ratio) * width_scale) + 0.5f);
        new_capture.m_Y = (uint16)((((float)img_rect.top * height_ratio) * height_scale) + 0.5f);
        new_capture.m_Width = (uint16)((((float)(img_rect.right - img_rect.left) * width_ratio) * width_scale) + 0.5f);
        new_capture.m_Height = (uint16)((((float)(img_rect.bottom - img_rect.top) * height_ratio) * height_scale) + 0.5f);

        // the final stats
        new_capture.m_Type = frame_type;
        new_capture.m_Size = new_capture.m_Width * new_capture.m_Height * sizeof(ImageRGB565);
        new_capture.m_Data = (byte*)malloc(new_capture.m_Size);

        // scale and convert the full size caches
        ScaleFrame(m_ScreenCache, frame_width, frame_height, m_ScaleCache, final_width, final_height);
        GetFinalFrame((ImageRGB565*)m_ScaleCache, new_capture, final_width, final_height);
        {
            LOCK_MUTEX(m_BufferLock); 
            if(m_FrameBuffer.size() >= BUFFER_SIZE)
            {
                do // free until a full frame
                {
                    CaptureData& front = m_FrameBuffer.front();
                    free(front.m_Data);
                    m_FrameBuffer.pop_front();
                } while(m_FrameBuffer.size() > 0 && m_FrameBuffer.front().m_Type == EFrameType::CHANGE);
            }
            m_FrameBuffer.push_back(new_capture);
        }
/*
        // This is here for reference if we need to see output
        static int count = 0;
        if(count++ < 10)
        {
            char buffer[64];
            sprintf(buffer, "src_%d_%s.jpg", count, frame_type == EFrameType::FULL ? "full" : "part");
            tje_encode_to_file(buffer, frame_width, frame_height, 4, m_ScreenCache);
 
            sprintf(buffer, "dest_%d_%s.jpg", count, frame_type == EFrameType::FULL ? "full" : "part");
            tje_encode_to_file(buffer, final_width, final_height, 2, m_ScaleCache);

            sprintf(buffer, "final_%d_%s.jpg", count, frame_type == EFrameType::FULL ? "full" : "part");
            tje_encode_to_file(buffer, new_capture.m_Height, new_capture.m_Width, 2, new_capture.m_Data);
        }
*/
    }
}

void CCaptureSCL::ScaleFrame(const byte* src, int32 src_width, int32 src_height, 
                            byte* dest, int32 dest_width, int32 dest_height)
{
    const byte* src_data[3] = { src, nullptr, nullptr };
    int32 src_stride[3]     = { src_width * (int32)sizeof(ImageBGRA), 0, 0 };
    byte* dest_data[3]      = { dest, nullptr, nullptr };
    int32 dest_stride[3]    = { dest_width * (int32)sizeof(ImageRGB565), 0, 0 };

    sws_scale(m_ScaleContext, src_data, src_stride, 0, src_height, dest_data, dest_stride);
}