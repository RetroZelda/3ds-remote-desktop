
#include "linux/Server_linux.h"

bool CServer_linux::Init(int16 sPort)
{
    CServer::Init(sPort);
}

void CServer_linux::Shutdown()
{
    CServer::Shutdown();

}

int32 CServer_linux::GetMyIP() const
{
    int32 ret = CServer::GetMyIP();
    struct ifaddrs *ifaddr, *ifa;
    int family, error;
    char host[NI_MAXHOST];

    if (getifaddrs(&ifaddr) == -1) 
    {
        perror("getifaddrs");
        exit(EXIT_FAILURE);
    }

    for (ifa = ifaddr; ifa != NULL; ifa = ifa->ifa_next) 
    {
        if (ifa->ifa_addr == NULL)
            continue;  

        error = getnameinfo(ifa->ifa_addr,sizeof(struct sockaddr_in),host, NI_MAXHOST, NULL, 0, NI_NUMERICHOST);
        if (error == 0 && ifa->ifa_addr->sa_family == AF_INET)
        {
            printf("Interface : <%s>\n\t  Address : <%s>\n",ifa->ifa_name, host );
        }
    }

    freeifaddrs(ifaddr);
    return ret;
}
