#include "linux/WindowData_linux.h"

#include <memory>

CWindowData_linux::CWindowData_linux(std::shared_ptr<SWindowHandle> handle) : IWindowData(handle)
{
    std::shared_ptr<SWindowHandle_linux> window = GetSharedHandle<SWindowHandle_linux>();
    SetDisplayText(window->WindowData.Name);
    UpdateWindowInfo();
}

CWindowData_linux::CWindowData_linux(SL::Screen_Capture::Window window) : 
    IWindowData(std::static_pointer_cast<SWindowHandle>(std::make_shared<SWindowHandle_linux>()))
{
    std::shared_ptr<SWindowHandle_linux> window_data = GetSharedHandle<SWindowHandle_linux>();
    window_data->WindowData = window;
    SetDisplayText(window_data->WindowData.Name);
    UpdateWindowInfo();
}

CWindowData_linux::~CWindowData_linux()
{
}


void CWindowData_linux::UpdateWindowInfo()
{
    std::shared_ptr<SWindowHandle_linux> window = GetSharedHandle<SWindowHandle_linux>();
    // a.Size.y << "  Width  " << a.Size.x << "   " << a.Name << std::endl;
    Rectangle64 rect;
    rect.left       = window->WindowData.Position.x;
    rect.top        = window->WindowData.Position.y;
    rect.right      = rect.left + window->WindowData.Size.x;
    rect.bottom     = rect.top + window->WindowData.Size.y;

    SetWindowRect(rect);
    SetWidth(window->WindowData.Size.x);
    SetHeight(window->WindowData.Size.y);    
    SetDisplayText(window->WindowData.Name);
}


void CWindowData_linux::ForceWindowOnTop(bool bForce)
{
    UpdateWindowInfo();
    std::shared_ptr<SWindowHandle_linux> window = GetSharedHandle<SWindowHandle_linux>();
    const Rectangle64& rect = GetWindowRect();

    // TODO: This
    if(bForce)
    {
    }
    else
    {
    }

}
