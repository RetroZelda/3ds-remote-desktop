
#include "linux/Timer_linux.h"

CTimer_linux::CTimer_linux()
{
	m_bIsRunning = false;
	m_dElapsedTime = 0.0;
	m_nFrameCount = 0;
	m_nFPS = 0;
}

CTimer_linux::~CTimer_linux()
{

}

void CTimer_linux::Start()
{
	if( !m_bIsRunning )
	{
		m_bIsRunning = true;
		Reset();
	}
}

void CTimer_linux::Stop()
{
	if( m_bIsRunning )
	{
		m_bIsRunning = false;
		m_dElapsedTime = GetElapsedTime();
	}
}

void CTimer_linux::Reset()
{
	m_dElapsedTime = 0.0;
	m_dFPSTimeStamp = 0.0f;
	clock_gettime(CLOCK_REALTIME, &m_StartTick);
    m_FrameTick = m_StartTick;
}

// TODO: THis functionality
void CTimer_linux::Pause()
{
}

// TODO: THis functionality
void CTimer_linux::Resume()
{
}

void CTimer_linux::Update()
{
	if( m_bIsRunning )
	{

		// query the tick count
		clock_gettime(CLOCK_REALTIME, &m_FrameTick);

		// convert to the time
		double dCurTime = (m_FrameTick.tv_sec + m_FrameTick.tv_nsec / 1000000000.0 - m_StartTick.tv_sec - m_StartTick.tv_nsec / 1000000000.0);
    
		m_dDeltaTime = dCurTime - m_dElapsedTime;
		m_dElapsedTime = dCurTime;

		// Count FPS
		m_nFrameCount++;

		// check if 1 second has passed
		if( m_dElapsedTime - m_dFPSTimeStamp > 1.0 )
		{
			// remember the frame count
			m_nFPS = m_nFrameCount;

			// reset the current frame count
			m_nFrameCount = 0;

			// get a new time stamp
			m_dFPSTimeStamp = m_dElapsedTime;
		}
	}
}

double CTimer_linux::GetElapsedTime() const
{
	return m_dElapsedTime;
}

double CTimer_linux::GetDeltaTime() const
{
	return m_dDeltaTime;
}

int CTimer_linux::FPS() const
{
	return m_nFPS;
}
