#include "linux/KeyboardCommand_linux.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>

namespace
{
    static TWEAKABLE char* KEYBOARD_PATH = (char*)"/dev/input/event0";
}

CKeyboardCommand_linux::CKeyboardCommand_linux() : 
    m_IsActive(false)
{
}

CKeyboardCommand_linux::~CKeyboardCommand_linux()
{

}

void CKeyboardCommand_linux::Init()
{
    CKeyboardCommand::Init();
    if(m_IsActive)
        return;

    memset(&m_KeyboardState, 0, sizeof(int16) * KEY_CNT);
    m_KeyboardHandle = open(KEYBOARD_PATH, O_RDONLY | O_NONBLOCK);
    if(m_KeyboardHandle > -1)
    {
        char buffer[256];
        ioctl(m_KeyboardHandle, EVIOCGNAME(256), buffer);

        m_Name = buffer;
        m_IsActive = true;
        m_Thread = std::make_unique<std::thread>(std::bind(&CKeyboardCommand_linux::ThreadTick, this));
    }
}

void CKeyboardCommand_linux::Shutdown()
{
    CKeyboardCommand::Shutdown();
    if(m_IsActive)
    {
        m_IsActive = false;
        m_Thread->join();
        m_Thread.reset();
    }
    close(m_KeyboardHandle);
}

void CKeyboardCommand_linux::SetKeyboardKeyState(int32 key_code, int16 key_state)
{

}

int16 CKeyboardCommand_linux::GetKeyboardKeyState(int32 key_code)
{
    if(key_code >= KEY_CNT)
        return 0;
    {
        LOCK_MUTEX(m_KeyMutex);
        return m_KeyboardState[key_code];
    }
}

void CKeyboardCommand_linux::ThreadTick()
{
    // kbd_dev should be keyboard device found in /dev/input (need root by default)
    input_event keyboard_event;

    while(m_IsActive)
    {
        int32 size = read(m_KeyboardHandle, &keyboard_event, sizeof(keyboard_event));
        if(size > 0)
        {
            if (keyboard_event.type & EV_KEY) 
            {
                LOCK_MUTEX(m_KeyMutex);
                // printf("key %d = %d\n", keyboard_event.code, keyboard_event.value);
                m_KeyboardState[keyboard_event.code] = keyboard_event.value;
            }
        }
    }
}