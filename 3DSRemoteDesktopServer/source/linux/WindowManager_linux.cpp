
#include "linux/WindowManager_linux.h"
#include "linux/WindowData_linux.h"

#include <ScreenCapture.h>

CWindowManager_linux::CWindowManager_linux() : 
    // TODO: Pass a proper window handle for the desktop... or a SL::Screen_Capture::Monitor
    IWindowManager(std::make_shared<CWindowData_linux>(SL::Screen_Capture::Window()))
{
    //SetDesktopWindow(std::make_shared<CWindowData_win32>(::GetDesktopWindow()))
}

CWindowManager_linux::~CWindowManager_linux()
{
}

void CWindowManager_linux::RefreshWindowList()
{
    std::vector<SL::Screen_Capture::Window> windows = SL::Screen_Capture::GetWindows();
    for (SL::Screen_Capture::Window &window : windows) 
    {
        AddWindow(std::make_shared<CWindowData_linux>(window));
    }
}
