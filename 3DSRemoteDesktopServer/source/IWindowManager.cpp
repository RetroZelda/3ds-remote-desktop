#include "IWindowManager.h"


IWindowManager::IWindowManager(std::shared_ptr<IWindowData> desktop_window)
{
    m_DesktopWindow = desktop_window;
}

IWindowManager::~IWindowManager()
{
    m_DesktopWindow.reset();
    ClearWindowList();
}

IWindowManager::iterator IWindowManager::iterator::next()
{
    iterator iter;
    iter.m_pWindowManager = m_pWindowManager;
    iter.m_pWindowIter = ++m_pWindowIter;
    return iter;
}

IWindowManager::iterator IWindowManager::iterator::prev()
{
    iterator iter;
    iter.m_pWindowManager = m_pWindowManager;
    iter.m_pWindowIter = --m_pWindowIter;
    return iter;
}

std::weak_ptr<IWindowData> IWindowManager::iterator::operator->() const
{
    return (*m_pWindowIter);
}

std::weak_ptr<IWindowData> IWindowManager::iterator::operator*() const
{
    return (*m_pWindowIter);
}

bool IWindowManager::iterator::operator!=(IWindowManager::iterator other)
{
    return other.m_pWindowIter != m_pWindowIter;
}

IWindowManager::iterator IWindowManager::begin()
{
    iterator iter;
    iter.m_pWindowManager = this;
    iter.m_pWindowIter = m_WindowList.begin();
    return iter;
}

IWindowManager::iterator IWindowManager::end()
{
    iterator iter;
    iter.m_pWindowManager = this;
    iter.m_pWindowIter = m_WindowList.end();
    return iter;
}

std::weak_ptr<IWindowData> IWindowManager::GetWindow(int nIndex) const
{
    return m_WindowList[nIndex];
}

std::weak_ptr<IWindowData> IWindowManager::operator[](int nIndex) const
{
    return m_WindowList[nIndex];
}

void IWindowManager::AddWindow(std::shared_ptr<IWindowData> window_data)
{
    m_WindowList.push_back(window_data);
}

void IWindowManager::ClearWindowList()
{
    while(m_WindowList.size() > 0)
    {
        m_WindowList.back().reset();
        m_WindowList.pop_back();
    }
}