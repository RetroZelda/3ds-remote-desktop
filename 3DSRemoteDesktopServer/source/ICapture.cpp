#include "ICapture.h"

#include "IWindowData.h"
#include <string.h> // for memcpy

#define BITS_5 0x1F
#define BITS_6 0x3F
#define BITS_8 0xFF
#define SHRINK_COLOR(in_val_8, out_val_max) ((uint32)((float)(in_val_8 / (float)BITS_8) * (float)(out_val_max)))

// TEMP
namespace
{
    // SL::Screen_Capture::ImageBGRA
    typedef struct ImageBGRA { unsigned char B, G, R, A; } ImageBGRA;
}

void ICapture::BGRA_to_RGB565(const byte* input, byte* output, int32 width, int32 height)
{
    ImageRGB565 destination_buffer;
    uint32 source_size = sizeof(ImageBGRA);
    uint32 destination_size = sizeof(ImageRGB565);
    // have to loop to rotate -90 degrees for the 3DS
    for(int y = 0; y < height; ++y)
    {
        ImageBGRA* pRow = (ImageBGRA*)((input) + (y * width));
        for(int x = 0; x < width; ++x)
        {
            destination_buffer ^= destination_buffer;
            destination_buffer |= SHRINK_COLOR(pRow->R, BITS_5) << 0xB; // R5
            destination_buffer |= SHRINK_COLOR(pRow->G, BITS_6) << 0x5; // G6
            destination_buffer |= SHRINK_COLOR(pRow->B, BITS_5); // B5
            memcpy(output + ((height * x + (height - y)) * destination_size), &destination_buffer, destination_size);
            ++pRow;
        }
    }
}