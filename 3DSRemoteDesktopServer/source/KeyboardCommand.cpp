
#include "KeyboardCommand.h"
#include "ServerSettings.h"
#include "Global.h"


CKeyboardCommand::CKeyboardCommand()
{
}

CKeyboardCommand::~CKeyboardCommand()
{
}

void CKeyboardCommand::Init()
{
    m_3DSKeyMap = CGlobal::ServerSettings->GetKeybindingMapRef();
}

void CKeyboardCommand::Shutdown()
{

}

void CKeyboardCommand::Translate3DSButtonToKeyState(uint32 BTN, int16 key_state)
{
    this->SetKeyboardKeyState(m_3DSKeyMap[BTN], key_state);
}

void CKeyboardCommand::Translate3DSAnalogToKeyState(int16 sX, int16 sY)
{
    int sDeadzone = CGlobal::ServerSettings->GetDeadzone();
    // left
    if(sX < -sDeadzone)
    {
        Translate3DSButtonToKeyState(CServerSettings::CTR_KEY_CPAD_LEFT, 1);
    }
    else
    {
        Translate3DSButtonToKeyState(CServerSettings::CTR_KEY_CPAD_LEFT, 0);
    }

    // right
    if(sX > sDeadzone)
    {
        Translate3DSButtonToKeyState(CServerSettings::CTR_KEY_CPAD_RIGHT, 1);
    }
    else
    {
        Translate3DSButtonToKeyState(CServerSettings::CTR_KEY_CPAD_RIGHT, 0);
    }

    // up
    if(sY > sDeadzone)
    {
        Translate3DSButtonToKeyState(CServerSettings::CTR_KEY_CPAD_UP, 1);
    }
    else
    {
        Translate3DSButtonToKeyState(CServerSettings::CTR_KEY_CPAD_UP, 0);
    }

    // down
    if(sY < -sDeadzone)
    {
        Translate3DSButtonToKeyState(CServerSettings::CTR_KEY_CPAD_DOWN, 1);
    }
    else
    {
        Translate3DSButtonToKeyState(CServerSettings::CTR_KEY_CPAD_DOWN, 0);
    }
}
