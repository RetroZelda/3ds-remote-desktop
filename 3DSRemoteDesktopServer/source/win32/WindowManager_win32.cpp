
#include "win32/WindowManager_win32.h"
#include "win32/WindowData_win32.h"

#include <windows.h>


BOOL CALLBACK enumWindowsProc( HWND hWnd, LPARAM lParam)
{
    CWindowManager_win32* pWM = (CWindowManager_win32*)lParam;
    int length = GetWindowTextLength( hWnd );
    if( length == 0 )
    {
        return true;
    }

    if(!IsWindowVisible(hWnd))
    {
        return true;
    }

    printf("Found window - %d\n", hWnd);
    std::shared_ptr<IWindowData> window = std::make_shared<CWindowData_win32>(hWnd);
    pWM->AddWindow(window);

    return true;
}


CWindowManager_win32::CWindowManager_win32() : 
    IWindowManager(std::make_shared<CWindowData_win32>(::GetDesktopWindow()))
{
}

CWindowManager_win32::~CWindowManager_win32()
{
}

void CWindowManager_win32::RefreshWindowList()
{
    EnumWindows( enumWindowsProc, (LPARAM)this);
}
