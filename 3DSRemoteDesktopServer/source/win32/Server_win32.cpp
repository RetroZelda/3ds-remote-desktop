#define _WIN32_WINNT 0x501
#include "win32/Server_win32.h"
#include "Global.h"
#include <stdio.h>


bool CServer_win32::Init(int16 sPort)
{
    printf("\nInitialising Winsock...");
    if (WSAStartup(MAKEWORD(2,2), &m_wsa) != 0)
    {
        printf("Failed. Error Code : %d\n", GETERRNO());
        return 1;
    }
    printf("Initialised.\n");

    CServer::Init(sPort);
}

void CServer_win32::Shutdown()
{
    CServer::Shutdown();

    WSACleanup();
}

int32 CServer_win32::GetMyIP() const
{
    int32 ret = CServer::GetMyIP();
    
    char ac[80];
    if (gethostname(ac, sizeof(ac)) == RNSOCKETERROR) 
    {
        return 1;
    }

    addrinfo in_addr;
    addrinfo* out_addr;

    memset(&in_addr, 0, sizeof(struct addrinfo));
           in_addr.ai_family = AF_INET;         /* Allow IPv4 */
           in_addr.ai_socktype = SOCK_STREAM;   /* TCP socket */
           in_addr.ai_flags = AI_PASSIVE;       /* For wildcard IP address */
           in_addr.ai_protocol = 0;             /* Any protocol */
           in_addr.ai_canonname = nullptr;
           in_addr.ai_addr = nullptr;
           in_addr.ai_next = nullptr;

    int32 error = getaddrinfo(ac, nullptr, &in_addr, &out_addr);
    if (error != 0) 
    {
        printf("Bad host lookup - error %d\n", error);
        return 1;
    }

    int32 count = 0;
    addrinfo* addr_list = out_addr;
    while(addr_list != nullptr)
    {
        printf("Address %d: %s\n", count++, inet_ntoa(((sockaddr_in*)addr_list->ai_addr)->sin_addr));
        addr_list = addr_list->ai_next;
    }
    freeaddrinfo(out_addr);

    return ret;
}
