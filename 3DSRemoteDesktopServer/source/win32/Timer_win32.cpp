
#include "win32/Timer_win32.h"

#define LEAN_AND_MEAN
#include <windows.h>

CTimer_win32::CTimer_win32()
{
	m_bIsRunning = false;
	m_dElapsedTime = 0.0;

	QueryPerformanceFrequency( (LARGE_INTEGER*)&m_llFrequency );
	QueryPerformanceCounter( (LARGE_INTEGER*)&m_llStartTick );
	m_dFPSTimeStamp	= GetTickCount();

	m_nFrameCount		= 0;
	m_nFPS				= 0;
}

CTimer_win32::~CTimer_win32()
{

}

void CTimer_win32::Start()
{
	if( !m_bIsRunning )
	{
		m_bIsRunning = true;
		Reset();
	}
}

void CTimer_win32::Stop()
{
	if( m_bIsRunning )
	{
		m_bIsRunning = false;

		m_dElapsedTime = GetElapsedTime();
	}
}

void CTimer_win32::Reset()
{
	m_dElapsedTime = 0.0;
	m_dFPSTimeStamp = 0;
	QueryPerformanceCounter( (LARGE_INTEGER*)&m_llStartTick );
}

// TODO: THis functionality
void CTimer_win32::Pause()
{
}

// TODO: THis functionality
void CTimer_win32::Resume()
{
}

void CTimer_win32::Update()
{
	if( m_bIsRunning )
	{

		// query the tick count
		int64	llStopTick;
		QueryPerformanceCounter( (LARGE_INTEGER*)&llStopTick );

		// convert to the time
		double dCurTime = ((double)(llStopTick - m_llStartTick) / (double)m_llFrequency) / 10000.0;

		m_dDeltaTime = dCurTime - m_dElapsedTime;
		m_dElapsedTime = dCurTime;

		// Count FPS
		m_nFrameCount++;

		// check if 1 second has passed
		if( m_dElapsedTime - m_dFPSTimeStamp > 1.0 )
		{
			// remember the frame count
			m_nFPS = m_nFrameCount;

			// reset the current frame count
			m_nFrameCount = 0;

			// get a new time stamp
			m_dFPSTimeStamp = m_dElapsedTime;
		}
	}
}

double CTimer_win32::GetElapsedTime() const
{
	return m_dElapsedTime;
}

double CTimer_win32::GetDeltaTime() const
{
	return m_dDeltaTime;
}

int CTimer_win32::FPS() const
{
	return m_nFPS;
}
