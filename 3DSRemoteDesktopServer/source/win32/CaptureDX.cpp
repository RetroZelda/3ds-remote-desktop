
#include "win32/CaptureDX.h"


#include <d3d9.h>
#include <d3dx9.h>
#include <d3dx9tex.h>

#include <winuser.h>
#include <string.h>

#include "Packet.h"
#include "win32/WindowData_win32.h"
//#include "Server.h"

#define BITSPERPIXEL 32
#define SAFE_FREE(x) if(x != NULL){free(x); x = NULL;}

#include <fstream>

namespace
{
    static TWEAKABLE uint16 BUFFER_SIZE = 5;
}

CCaptureDX::CCaptureDX(void)
{
    // HWND m_DesktopWindowHandle = GetDesktopWindow();
}

CCaptureDX::~CCaptureDX(void)
{
}

void CCaptureDX::Init(std::weak_ptr<IWindowData> window)
{

	// // get the monitor the windows is a part of
	// HMONITOR monitor = MonitorFromWindow(m_pWindowHandle, MONITOR_DEFAULTTOPRIMARY);
	// MONITORINFO monitorInfo;
	// monitorInfo.cbSize = sizeof(MONITORINFO);
	// BOOL bGotMonitorInfo = GetMonitorInfo(monitor, &monitorInfo);

    // // fill in the capture info
	// // the screen size
	// m_unScreenWidth = monitorInfo.rcMonitor.right - monitorInfo.rcMonitor.left;
	// m_unScreenHeight = monitorInfo.rcMonitor.bottom - monitorInfo.rcMonitor.top;
    // memcpy(&m_ScreenRect, &monitorInfo.rcMonitor, sizeof(RECT));

    m_pWindow = dynamic_cast<CWindowData_win32*>(window.lock().get());
	// if(auto spHandle = m_pWindow->GetHandle().lock())
	// {
    // 	GetClientRect(*dynamic_cast<SWindowHandle_win32*>(spHandle.get()), &m_ScreenRect);
	// }
	// else
	{
    	GetClientRect(GetDesktopWindow(), &m_ScreenRect);
	}
	m_unScreenWidth = m_ScreenRect.right - m_ScreenRect.left;
	m_unScreenHeight = m_ScreenRect.bottom - m_ScreenRect.top;


	// create the interface
	m_pInterface = Direct3DCreate9(D3D_SDK_VERSION);

	if(m_pInterface == NULL)
	{
		// couldnt create interface
		return;
	}

	HWND window_handle;
	if(auto spHandle = m_pWindow->GetHandle<SWindowHandle_win32>().lock())
	{
		window_handle = spHandle->WindowHandle;
	}
	else
	{
		// invalid handle
		return;
	}

	// create the present params
	D3DPRESENT_PARAMETERS presentParams;
	memset(&presentParams, 0, sizeof(D3DPRESENT_PARAMETERS));
	presentParams.Windowed = true;
	presentParams.SwapEffect = D3DSWAPEFFECT_COPY;
	presentParams.BackBufferWidth = m_unScreenWidth;
	presentParams.BackBufferHeight = m_unScreenHeight;
	presentParams.hDeviceWindow = window_handle;

	// create the device
	UINT adapter = D3DADAPTER_DEFAULT;
	D3DDEVTYPE deviceType = D3DDEVTYPE_HAL;
	DWORD behaviorFlags = D3DCREATE_HARDWARE_VERTEXPROCESSING;

	HRESULT error = m_pInterface->CreateDevice(adapter, deviceType, window_handle, behaviorFlags, &presentParams, &m_pDevice);
	if(FAILED(error))
	{
		return;
	}


    // keep window on top
    m_pWindow->ForceWindowOnTop(true);

	// create the surface to draw into
	m_pDevice->CreateOffscreenPlainSurface(m_unScreenWidth, m_unScreenHeight, D3DFMT_A8R8G8B8, D3DPOOL_SYSTEMMEM, &m_pBufferSurface, NULL);
	m_pDevice->CreateOffscreenPlainSurface(m_pSettings.m_unDestinationWidth, m_pSettings.m_unDestinationHeight,
                                        D3DFMT_R5G6B5, D3DPOOL_SYSTEMMEM, &m_pFinalSurface, NULL);

	return;
}

void CCaptureDX::Shutdown()
{
    m_pWindow->ForceWindowOnTop(false);

	// shutdown DirectX stuff
	if(m_pBufferSurface != NULL)
	{
		m_pBufferSurface->Release();
		m_pBufferSurface = NULL;
	}
	if(m_pFinalSurface != NULL)
	{
		m_pFinalSurface->Release();
		m_pFinalSurface = NULL;
	}
	if(m_pDevice != NULL)
	{
		m_pDevice->Release();
		m_pDevice = NULL;
	}
	if(m_pInterface != NULL)
	{
		m_pInterface->Release();
		m_pInterface = NULL;
	}
}

void CCaptureDX::Start()
{
	m_IsRunning = true;
}

void CCaptureDX::Resume()
{
	m_IsPaused = false;
}

void CCaptureDX::Pause()
{
	m_IsPaused = true;
}

void CCaptureDX::Stop()
{
    // clear the buffer
    while(m_FrameBuffer.size() > 0)
    {
        CaptureData& front = m_FrameBuffer.front();
        free(front.m_Data);
        m_FrameBuffer.pop_front();
    }
	m_IsRunning = false;
}

uint32 CCaptureDX::GetNextFrame(CaptureData& out_data)
{
    if(m_FrameBuffer.size() > 0)
    {
        out_data = m_FrameBuffer.front();
        m_FrameBuffer.pop_front();

        return out_data.m_Size;
    }
    return CaptureFrame(out_data);
}

// TODO: Make this run on its own thread 
uint16 CCaptureDX::CaptureFrame(CaptureData& out_data)
{
	HRESULT err = m_pDevice->GetFrontBufferData(0, m_pBufferSurface);
	//HRESULT err = m_pDevice->GetBackBuffer(0, 0, D3DBACKBUFFER_TYPE_MONO, &m_pBufferSurface);
	if(err != D3D_OK)
	{
		switch(err)
		{
			case D3DERR_DRIVERINTERNALERROR:
				printf("D3DERR_DRIVERINTERNALERROR - ");
				break;
			case D3DERR_DEVICELOST:
				printf("D3DERR_DEVICELOST - ");
				break;
			case D3DERR_INVALIDCALL:
				printf("D3DERR_INVALIDCALL - ");
				break;
		}
		printf("(%d) - Error on GetBackBuffer()\n", err);
		return 0;
	}

	m_pWindow->UpdateWindowInfo();
	RECT windowRect = m_pWindow->GetWindowRect();

	err = D3DXLoadSurfaceFromSurface(m_pFinalSurface, NULL, NULL,
		m_pBufferSurface, NULL, &windowRect, D3DX_FILTER_LINEAR, 0);
	if(err != D3D_OK)
	{
		int32 invc = D3DERR_INVALIDCALL;
		int32 invd = D3DXERR_INVALIDDATA;
		printf("(%d) - Error on D3DXLoadSurfaceFromSurface()\n", err);
		return 0;
	}


	D3DSURFACE_DESC finalDesc;
	m_pFinalSurface->GetDesc(&finalDesc);

	int32 nW = finalDesc.Width;
	int32 nH = finalDesc.Height;
	int32 nPixelSize = sizeof(ImageRGB565);

	// setup return data
	out_data.m_Size = nW * nH * nPixelSize;
	out_data.m_Data = (byte*)malloc(out_data.m_Size);
	out_data.m_Type = EFrameType::FULL;
	out_data.m_X = m_pWindow->GetWindowRect().left;
	out_data.m_Y = m_pWindow->GetWindowRect().top;
	out_data.m_Width = nW;
	out_data.m_Height = nH;


	// RGB_565
	// Read the pixels
	D3DLOCKED_RECT bits;
	uint32 nCurOffset = 0;
	if(SUCCEEDED(m_pFinalSurface->LockRect(&bits, 0, D3DLOCK_READONLY)))
	{
		// have to loop to rotate -90 degrees
		for(int32 y = 0; y < nH; ++y)
		{
			uint16* pRow = (uint16*)(((BYTE*)bits.pBits) + (y * bits.Pitch));
			for(int32 x = 0; x < nW; ++x)
			{
				uint16* dest = (uint16*)(out_data.m_Data + ((nH * x + (nH - y)) * nPixelSize) - nPixelSize);
				if((uint32)dest > (uint32)(out_data.m_Data + out_data.m_Size - nPixelSize))
					printf("***problem***\n");
				*dest = *(pRow++);
				nCurOffset += 3;
			}
		}
		m_pFinalSurface->UnlockRect();
		return out_data.m_Size;
	}
	return 0;
}

// TODO: Make this run on its own thread 
void CCaptureDX::Temp_Update()
{
	CaptureData new_capture;
	uint16 capture_size = CaptureFrame(new_capture);
	if(capture_size > 0)
	{
		// dont allow more than the set ammount in the queue
		if(m_FrameBuffer.size() >= BUFFER_SIZE)
		{
			
			do // free until a full frame
			{
				CaptureData& front = m_FrameBuffer.front();
				free(front.m_Data);
				m_FrameBuffer.pop_front();
			} while(m_FrameBuffer.front().m_Type == EFrameType::CHANGE);
		}
		m_FrameBuffer.push_back(new_capture);
	}
}