#include "win32/WindowData_win32.h"

#include <windows.h>
#include <memory>

CWindowData_win32::CWindowData_win32(std::shared_ptr<SWindowHandle> handle) : IWindowData(handle)
{
    char buffer[64];
    memset( &buffer, 0, 64);

    HWND window_handle = GetSharedHandle<SWindowHandle_win32>()->WindowHandle;
    if(::GetWindowText(window_handle, buffer, 64) == 0)
    {
        int32 error = GetLastError();
        sprintf(buffer, "Couldnt get window(%d) text. err 0X%X(%d)", window_handle, error, error);
    }
    this->SetDisplayText(buffer);
    UpdateWindowInfo();
}

CWindowData_win32::CWindowData_win32(HWND handle) 
: IWindowData(std::static_pointer_cast<SWindowHandle>(std::make_shared<SWindowHandle_win32>()))
{

    char buffer[64];
    memset( &buffer, 0, 64);
    GetSharedHandle<SWindowHandle_win32>()->WindowHandle = handle;
    if(::GetWindowText(handle, buffer, 64) == 0)
    {
        int32 error = GetLastError();
        sprintf(buffer, "Couldnt get window(%d) text. err 0X%X(%d)", handle, error, error);
    }
    this->SetDisplayText(buffer);
    UpdateWindowInfo();
}

CWindowData_win32::~CWindowData_win32()
{
}


void CWindowData_win32::UpdateWindowInfo()
{
    Rectangle64 pos, rect;
    HWND handle = GetSharedHandle<SWindowHandle_win32>()->WindowHandle;
    GetClientRect(handle, &rect);
    memcpy(&pos, &rect, sizeof(RECT));
    MapWindowPoints(GetParent(handle), (handle), (LPPOINT)&pos, 2);

    rect.left -= pos.left;
    rect.top -= pos.top;
    rect.right -= pos.left;
    rect.bottom -= pos.top;
    SetWindowRect(rect);
}


void CWindowData_win32::ForceWindowOnTop(bool bForce)
{
    UpdateWindowInfo();
    HWND handle = GetSharedHandle<SWindowHandle_win32>()->WindowHandle;
    const Rectangle64& rect = GetWindowRect();
    if(bForce)
    {
        SetFocus(handle);
        SetWindowPos(handle, HWND_TOPMOST,
                 rect.left, rect.top, rect.right - rect.left, rect.bottom - rect.top,
                 0);
    }
    else
    {
        SetWindowPos(handle, HWND_NOTOPMOST,
                 rect.left, rect.top, rect.right - rect.left, rect.bottom - rect.top,
                 0);
    }

}
