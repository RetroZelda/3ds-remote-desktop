#include "win32/KeyboardCommand_win32.h"

#include <windows.h>

CKeyboardCommand_win32::CKeyboardCommand_win32()
{

}

CKeyboardCommand_win32::~CKeyboardCommand_win32()
{

}

void CKeyboardCommand_win32::Init()
{
    CKeyboardCommand::Init();
}

void CKeyboardCommand_win32::Shutdown()
{
    CKeyboardCommand::Shutdown();
}

void CKeyboardCommand_win32::SetKeyboardKeyState(int32 key_code, int16 key_state)
{
    keybd_event( (BYTE)key_code, MapVirtualKeyA(key_code, MAPVK_VK_TO_VSC), KEYEVENTF_EXTENDEDKEY | ((key_state != 1) ? KEYEVENTF_KEYUP : 0), 0 );
}

int16 CKeyboardCommand_win32::GetKeyboardKeyState(int32 cKeyCode)
{
    return GetAsyncKeyState(cKeyCode);
}