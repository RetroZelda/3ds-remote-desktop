#include "Global.h"

CSingleton<ITimer> CGlobal::Timer;
CSingleton<CServer> CGlobal::Server;
CSingleton<ICapture> CGlobal::ScreenCapture;
CSingleton<IWindowManager> CGlobal::WindowManager;
CSingleton<CServerSettings> CGlobal::ServerSettings;
CSingleton<CKeyboardCommand> CGlobal::KeyboardCommand;

void CGlobal::Init()
{
#if defined(WIN32)

    Timer.Create<CTimer_win32>();
    Server.Create<CServer_win32>();
    ScreenCapture.Create<CCaptureDX>();
    WindowManager.Create<CWindowManager_win32>();
    KeyboardCommand.Create<CKeyboardCommand_win32>();

#elif defined(LINUX)

    Timer.Create<CTimer_linux>();
    Server.Create<CServer_linux>();
    ScreenCapture.Create<CCaptureSCL>();
    WindowManager.Create<CWindowManager_linux>();
    KeyboardCommand.Create<CKeyboardCommand_linux>();

#elif defined(OSX)
#endif // platform block

    CGlobal::ServerSettings.Create<CServerSettings>();
}

void CGlobal::Shutdown()
{
    Timer.Destroy();
    Server.Destroy();
    ScreenCapture.Destroy();
    WindowManager.Destroy();
    ServerSettings.Destroy();
    KeyboardCommand.Destroy();
}