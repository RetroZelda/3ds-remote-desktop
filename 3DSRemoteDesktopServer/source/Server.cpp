
#include "Server.h"
#include "Global.h"
#include <iostream>
#include <stdio.h>

namespace
{
    // TODO: GET THESE OUT OF HERE!!!!!!!!
    int nFakeState = -1;
}


CServer::CServer()
{
    m_bRunning = false;
}

CServer::~CServer()
{
}

// retrieved from internet somewhere
int32 CServer::GetMyIP() const
{
    char ac[80];
    if (gethostname(ac, sizeof(ac)) == RNSOCKETERROR) 
    {
        printf("Error %d: when getting local host name.\n", GETERRNO());
        return 1;
    }
    printf("Host Name: %s\n", ac);
    printf("Bound Address: %s\n", inet_ntoa(m_ServerAddr.sin_addr));

    return 0;
}


void CServer::StartHandshake()
{
    m_dHandShakeTime = CGlobal::Timer->GetElapsedTime();
    SendData(PACKET_HANDSHAKEREQUEST, 0, NULL);
}


void CServer::SendData(uint32 nPacketID, uint32 nDataSize, byte* Data)
{
    if(nPacketID == PACKET_SCREENRETRIEVED)
    {
        m_bReadyForScreen = false;
    }

    // send
    printf("[send]");
    int32 nSendAmmount = RNSendData(m_ClientSocket, nPacketID, nDataSize, Data);
    printf("[size: %d][type: %d]\n", nSendAmmount, nPacketID);
    if(nSendAmmount == RNSOCKETERROR)
    {
        int32 nErrorCode = GETERRNO();
        // TODO: Build packet queue and only send when peek() is kosher
        if(nErrorCode != RNEWOULDBLOCK)
        {
            printf("Problem contacting the client or client disconnected.  Error Code Ox%X(%d)\n", nErrorCode, nErrorCode);
            RNCloseSocket(m_ClientSocket);
            nFakeState = -1;
        }
    }
}

void CServer::RecievePacketData()
{
    int32 error = 0;
    Packet* pRecvPacket;
    int32 ret = RNRecieveData(m_ClientSocket, &pRecvPacket, &error);
    if(ret > 0)
    {
        Dispatch(pRecvPacket);
        free(pRecvPacket);
    }
    else if(ret == 0)
    {
        printf("The Client Disconnected.\n");
        nFakeState = -1;
        RNCloseSocket(m_ClientSocket);
        m_bReadyForScreen = false;
    }
    else
    {
        int32 errorNumber = GETERRNO();
        if(errorNumber != RNEWOULDBLOCK && errorNumber != RNCONNRESET)
        {
            printf("An error has occurred or the client timed out.  Error Code Ox%X(%d)\n", errorNumber, errorNumber);
            RNCloseSocket(m_ClientSocket);
            nFakeState = -1;
        }
    }
}

void CServer::Dispatch(Packet* pPacket)
{
    int32 ret = sizeof(Packet) + pPacket->_PacketSize;
    uint32 nInput = 0;
    switch(pPacket->_PacketID)
    {
    case PACKET_INPUT_PRESSED:
        memcpy(&nInput, pPacket->_PacketData, sizeof(uint32));
        printf("[recv][size: %d][type: %d][key: %u]\n", ret, pPacket->_PacketID, nInput);
        CGlobal::KeyboardCommand->Translate3DSButtonToKeyState(nInput, true);
        break;
    case PACKET_INPUT_RELEASED:
        memcpy(&nInput, pPacket->_PacketData, sizeof(uint32));
        printf("[recv][size: %d][type: %d][key: %u]\n", ret, pPacket->_PacketID, nInput, nInput);
        CGlobal::KeyboardCommand->Translate3DSButtonToKeyState(nInput, false);
        break;
    case PACKET_INPUT_CIRCLEPAD:
        printf("[recv][size: %d][type: %d][x: %d | y: %d]\n", ret, pPacket->_PacketID, ((int16*)pPacket->_PacketData)[0], ((int16*)pPacket->_PacketData)[1]);

        CGlobal::KeyboardCommand->Translate3DSAnalogToKeyState(((int16*)pPacket->_PacketData)[0], ((int16*)pPacket->_PacketData)[1]);

        break;
    case PACKET_SCREEN:
        printf("[recv][size: %d][type: %d]\n", ret, pPacket->_PacketID);
        break;
    case PACKET_SCREENRETRIEVED:
        printf("[recv][size: %d][type: %d]\n", ret, pPacket->_PacketID);
        m_bReadyForScreen = true;
        break;
    case PACKET_HANDSHAKEREQUEST:
        SendData(PACKET_HANDSHAKERESPONSE, 0, NULL);
        printf("[recv][size: %d][type: %d]\n", ret, pPacket->_PacketID);
        break;
    case PACKET_HANDSHAKERESPONSE:
        {
            printf("[recv][size: %d][type: %d]\n", ret, pPacket->_PacketID);
            double dCurTime = CGlobal::Timer->GetElapsedTime();
            double dDelta = dCurTime - m_dHandShakeTime;
            printf("Handshake Response Time: %f ms\n", dDelta);
        }
        break;
    case PACKET_MESSAGE:
        printf("[recv][size: %d][type: %d][message: %s]\n", ret, pPacket->_PacketID, (char*)&pPacket->_PacketData);
        break;
    };
}

bool CServer::Init(int16 sPort)
{
    if((m_ServerSocket = socket(AF_INET , SOCK_STREAM , 0 )) == RNBADSOCKET)
    {
        printf("Could not create socket : %d\n" , GETERRNO());
    }
    printf("Socket created.\n");

    //Prepare the sockaddr_in structure
    m_ServerAddr.sin_family = AF_INET;
    m_ServerAddr.sin_addr.s_addr = INADDR_ANY;
    m_ServerAddr.sin_port = htons( sPort );

    //Bind the socket to make it a server
    if( bind(m_ServerSocket ,(struct sockaddr *)&m_ServerAddr , sizeof(m_ServerAddr)) == RNSOCKETERROR)
    {
        printf("Bind failed with error code : %d\n" , GETERRNO());
    }
    printf("Bind done. using port %d \n", sPort);

    // stop blocking
    uint64 NoBlock = 1;
    RNioctl(m_ServerSocket, FIONBIO, &NoBlock);

    // start listening
    listen(m_ServerSocket, 3);
    m_bRunning = true;

    // start any utilities
    m_bReadyForScreen = false;

    return 0;

}

bool CServer::HasClient() const
{
    return nFakeState == 1;
}


bool CServer::ReadyForScreen()
{
    return m_bReadyForScreen;
}

void CServer::Run()
{
    switch(nFakeState)
    {
    case -1:
        printf("Awaiting connection... ");
        nFakeState = 0;
        break;
    case 0: // wait for connection
        {
            int32 c = sizeof(m_ClientAddr);
            m_ClientSocket = accept(m_ServerSocket, (struct sockaddr *)&m_ClientAddr, (socklen_t*)&c);
            if (m_ClientSocket != RNBADSOCKET)
            {
                // uint64 NoBlock = 1;
                // RNioctl(m_ClientSocket, FIONBIO, &NoBlock);
                printf("established\n");

                // handshake hte client
                StartHandshake();
                nFakeState = 1;
            }
            else
            {
                // printf("accept failed with error code : %d\n" , GETERRNO());
            }
        }
        break;
    case 1: // has connection
        {
            RecievePacketData();


            double dCurTime = CGlobal::Timer->GetElapsedTime();
            double dDelta = dCurTime - m_dHandShakeTime;
            if(dDelta > 15000)
            {
                StartHandshake();
            }
        }

        break;
    default: // dafuq
        break;
    };
}

void CServer::Shutdown()
{
    if(!m_bRunning)
    {
        return;
    }

    // disconnect all clients
    if(m_ClientSocket != RNBADSOCKET)
    {
        shutdown(m_ClientSocket, 0);
        RNCloseSocket(m_ClientSocket);
    }

    RNCloseSocket(m_ServerSocket);
    m_bRunning = false;
}
