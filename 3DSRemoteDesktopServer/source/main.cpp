
#include "Global.h"

#include <stdio.h>
#include <lz4.h>
#include <vector>
#include <iostream>

#if defined(LINUX)
#include <unistd.h> // for getcwd
#include <linux/limits.h> // for PATH_MAX
#endif


ScreenPacketData* CaptureScreenPacket()
{
    // poll the screen
    CaptureData screen_data;
    unsigned int nScreenSize = CGlobal::ScreenCapture->GetNextFrame(screen_data);
    if(nScreenSize > 0)
    {
        // compress the screen data into the final packet
        // NOTE: we are allocate enough for hte full uncompressed so we know it will fit
        ScreenPacketData* pCompressedPacket = (ScreenPacketData*)malloc(sizeof(ScreenPacketData) + nScreenSize);
        int nCompressedSize = LZ4_compress_default((char*)screen_data.m_Data, (char*)pCompressedPacket->Data.PixelData, nScreenSize, nScreenSize);

        if(nCompressedSize > 0)
        {
            printf("Compressed packet %d/%d(%d%%)\n", nCompressedSize, nScreenSize, (int)(((float)nCompressedSize / (float)nScreenSize) * 100.0f) );

            // set the final packet data before send
            pCompressedPacket->Data.X           = screen_data.m_X;
            pCompressedPacket->Data.Y           = screen_data.m_Y;
            pCompressedPacket->TotalSize        = nScreenSize;
            pCompressedPacket->Data.Width       = screen_data.m_Width;
            pCompressedPacket->Data.Height      = screen_data.m_Height;
            pCompressedPacket->CompressedSize   = nCompressedSize;
        }
        else
        {
            printf("Problem compressing screen(%d)\n", nCompressedSize);
            free(pCompressedPacket);
            return NULL;
        }


        // free the data
        free(screen_data.m_Data);
        return pCompressedPacket;
    }
}

int main(int32 argc, char* argv[])
{
#if defined(LINUX)
    // set our working dir to the target dir from argv[0]
    if(argc > 0)
    {
        std::string path = argv[0];
        std::size_t position = path.find_last_of("/");
        if(position == std::string::npos)
            position = path.find_last_of("\\");
        if(position != std::string::npos)
        {
            path.resize(position + 1);
            if(chdir(path.c_str()) != 0)
            {
                printf("Unable to change working directory. Error %d\n", errno);
            }
        }
    }
    char working_directory[PATH_MAX];
    if(getcwd(working_directory, PATH_MAX) != nullptr)
    {
        printf("Working dir: %s\n", working_directory);
    }
#endif // LINUX

    CGlobal::Init();
    CGlobal::Timer->Start();

    printf("LZ4 Library version = %d\n", LZ4_versionNumber());

	printf("Loading Server Settings - config.ini\n");
	CGlobal::ServerSettings->LoadSettings("config.ini");
    
	printf("Initializing Keyboard\n");
    CGlobal::KeyboardCommand->Init();

	printf("Generating list of capturable windows...\n");
    CGlobal::WindowManager->RefreshWindowList();

    IWindowManager::iterator windowIter = CGlobal::WindowManager->begin();
    int nCount = 0;
    for(; windowIter != CGlobal::WindowManager->end(); windowIter = windowIter.next())
    {
        if(auto spIterData = (*windowIter).lock())
        {
            printf("  %d - %s\n", nCount++, spIterData->GetDisplayText().c_str());
        }
    }

#if defined(WIN32)
	printf("  -1) Entire Desktop\n\n");
#endif // WIN32
	printf("\t\tSelect a window to capture: ");

	int nResult = 0;
	std::cin >> nResult;

    std::shared_ptr<IWindowData> pFocusWindow;
    if(nResult >= 0 && nResult < CGlobal::WindowManager->size())
    {
        pFocusWindow = CGlobal::WindowManager->GetWindow(nResult).lock();
        printf("\n***** Capturing %s! *****\n", pFocusWindow->GetDisplayText().c_str());
    }
    else
    {
#if defined(WIN32)
        pFocusWindow = CGlobal::WindowManager->GetDesktopWindow().lock();
        printf("\n***** Capturing Desktop! *****\n");
#else
        pFocusWindow = CGlobal::WindowManager->GetWindow(0).lock();
        printf("\n***** Invalid Window - Defaulting to %s *****\n", pFocusWindow->GetDisplayText().c_str());
#endif // WIN32
    }

    // init
	printf("Initializing the Capture Device...\n");
	CGlobal::ScreenCapture->Init(pFocusWindow);
	printf("Initialization Complete!\n");

    CGlobal::Server->Init(CGlobal::ServerSettings->GetPort());
    CGlobal::Server->GetMyIP();

    // start
    CGlobal::ScreenCapture->Start();

    // run the server
    bool bExit = false;
    while(!bExit)
    {
        CGlobal::Timer->Update();
        CGlobal::Server->Run();

        if(CGlobal::Server->HasClient() )
        {
            #if defined(WIN32)
            // capture on this thread temporarily
            dynamic_cast<CCaptureDX*>(*CGlobal::ScreenCapture)->Temp_Update();

            #endif
            if(CGlobal::Server->ReadyForScreen())
            {
                ScreenPacketData* pPacket = CaptureScreenPacket();

                if(pPacket != NULL)
                {
                    // send it
                    int nFinalPacketSize = sizeof(ScreenPacketData) + pPacket->CompressedSize;
                    CGlobal::Server->SendData(PACKET_SCREEN, nFinalPacketSize, (byte*)pPacket);
                    free(pPacket);
                }
            }
        }
#if defined(WIN32)
        if(CGlobal::KeyboardCommand->GetKeyboardKeyState(VK_ESCAPE))
#elif defined(LINUX)
        if(CGlobal::KeyboardCommand->GetKeyboardKeyState(KEY_ESC))
#endif // platform block
        {
            bExit = true;
            printf("\nExiting...\n");
        }
    }
    CGlobal::Server->Shutdown();
    CGlobal::ScreenCapture->Stop();
    CGlobal::ScreenCapture->Shutdown();
    CGlobal::KeyboardCommand->Shutdown();
    CGlobal::WindowManager->ClearWindowList();
    CGlobal::Timer->Stop();
    CGlobal::Shutdown();
}
