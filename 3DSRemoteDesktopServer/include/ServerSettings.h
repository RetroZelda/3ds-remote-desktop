
#ifndef _SERVERSETTINGS_H_
#define _SERVERSETTINGS_H_

#include "Common.h"
#include <string>
#include <map>


struct ImageInfo
{
    int imageWidth;
    int imageHeight;
};

class CCaptureDX;
class CServerSettings
{
	friend class CCaptureDX;
	//friend class CVitaInput;

public:
    // TODO: include proper header file
    #define BIT(n) (1U<<(n)) // frop "types.h"
    // from "hid.h"
    enum PAD_KEY
    {
        CTR_KEY_A       = BIT(0),
        CTR_KEY_B       = BIT(1),
        CTR_KEY_SELECT  = BIT(2),
        CTR_KEY_START   = BIT(3),
        CTR_KEY_DRIGHT  = BIT(4),
        CTR_KEY_DLEFT   = BIT(5),
        CTR_KEY_DUP     = BIT(6),
        CTR_KEY_DDOWN   = BIT(7),
        CTR_KEY_R       = BIT(8),
        CTR_KEY_L       = BIT(9),
        CTR_KEY_X       = BIT(10),
        CTR_KEY_Y       = BIT(11),
        CTR_KEY_ZL      = BIT(14), // (new 3DS only)
        CTR_KEY_ZR      = BIT(15), // (new 3DS only)
        CTR_KEY_TOUCH   = BIT(20), // Not actually provided by HID
        CTR_KEY_CSTICK_RIGHT = BIT(24), // c-stick (new 3DS only)
        CTR_KEY_CSTICK_LEFT  = BIT(25), // c-stick (new 3DS only)
        CTR_KEY_CSTICK_UP    = BIT(26), // c-stick (new 3DS only)
        CTR_KEY_CSTICK_DOWN  = BIT(27), // c-stick (new 3DS only)
        CTR_KEY_CPAD_RIGHT = BIT(28), // circle pad
        CTR_KEY_CPAD_LEFT  = BIT(29), // circle pad
        CTR_KEY_CPAD_UP    = BIT(30), // circle pad
        CTR_KEY_CPAD_DOWN  = BIT(31), // circle pad

        // Generic catch-all directions
        CTR_KEY_UP    = CTR_KEY_DUP    | CTR_KEY_CPAD_UP,
        CTR_KEY_DOWN  = CTR_KEY_DDOWN  | CTR_KEY_CPAD_DOWN,
        CTR_KEY_LEFT  = CTR_KEY_DLEFT  | CTR_KEY_CPAD_LEFT,
        CTR_KEY_RIGHT = CTR_KEY_DRIGHT | CTR_KEY_CPAD_RIGHT,
    };

	CServerSettings(void);
	~CServerSettings(void);

    bool LoadSettings(std::string file_name);

    uint32 GetDestinationWidth() const { return m_unDestinationWidth; }
    uint32 GetDestinationHeight() const { return m_unDestinationHeight; }
    int16 GetPort() const { return m_sPort;}
    int16 GetDeadzone() const { return m_sDeadzone;}
    std::map<uint32, int32>& GetKeybindingMapRef() { return m_KeyBinding;}

private:
    std::map<uint32, int32> m_KeyBinding;
    int16 m_sDeadzone;
    int16 m_sPort;
	uint32 m_unDestinationWidth;
	uint32 m_unDestinationHeight;

	Rectangle64 m_DestinationRect;

public:
};

#endif
