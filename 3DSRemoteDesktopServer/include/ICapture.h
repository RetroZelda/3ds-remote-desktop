#ifndef _ICAPTURE_
#define _ICAPTURE_

#include "IWindowData.h"
#include "Common.h"
#include "Types.h"

typedef uint16 ImageRGB565;

enum EFrameType : uint8 { FULL, CHANGE };

struct CaptureData
{
    uint16      m_X;
    uint16      m_Y;
    uint16      m_Width;
    uint16      m_Height;
    EFrameType  m_Type;
    uint32      m_Size;
    byte*       m_Data;
};

class ICapture
{
public:
	virtual void Init(std::weak_ptr<IWindowData> window) = 0;
	virtual void Shutdown() = 0;

    virtual void Start() = 0;
    virtual void Resume() = 0;
    virtual void Pause() = 0;
    virtual void Stop() = 0;

	virtual uint32 GetNextFrame(CaptureData& out_data) = 0;

    virtual bool IsRunning() const = 0;
    virtual bool IsPaused() const  = 0;

protected:

    static void BGRA_to_RGB565(const byte* input, byte* output, int32 width, int32 height);
private:
};

#endif // _ICAPTURE_