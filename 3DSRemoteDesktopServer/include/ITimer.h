
#ifndef _ITIMER_H_
#define _ITIMER_H_

#include "Types.h"

class ITimer
{
public:
	//stop watch functions
	virtual void Start() = 0;
	virtual void Stop() = 0;
	virtual void Update() = 0;
	virtual void Reset() = 0;

	// modifiers
	virtual void Pause() = 0;
	virtual void Resume() = 0;

	// accessors
	virtual double GetElapsedTime() const = 0;
	virtual double GetDeltaTime() const = 0;
	virtual int32 FPS() const = 0;
};
#endif // _ITIMER_H_