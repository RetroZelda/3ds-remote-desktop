#ifndef _THREADING_
#define _THREADING_

#include <chrono>
#include <thread>
#include <mutex>

#define LOCK_MUTEX(m) std::lock_guard<std::mutex> _guard_##m(m)

typedef std::mutex MUTEX;

#endif // _THREADING_