
#ifndef _SERVER_H_
#define _SERVER_H_

#include "KeyboardCommand.h"
#include "RetroNetwork.h"
#include "Packet.h"
#include "Common.h"


class CServer
{
public:

    CServer();
    ~CServer();

    virtual bool Init(int16 sPort);
    virtual void Shutdown();
    virtual int32 GetMyIP() const;
    void Run();

    void SendData(uint32 nPacketID, uint32 nDataSize, byte* Data);

    bool HasClient() const;
    bool ReadyForScreen();


protected:
    void StartHandshake();
    void RecievePacketData();
    void Dispatch(Packet* pPacket);

private:
    double m_dHandShakeTime;
    bool m_bReadyForScreen;
    bool m_bRunning;

    RNSocket m_ServerSocket;
    RNSocket m_ClientSocket;
    sockaddr_in m_ServerAddr;
    sockaddr_in m_ClientAddr;
};

#endif // _SERVER_H_
