#ifndef _GLOBAL_
#define _GLOBAL_

#include "Server.h" // ensures winsock in included before windows
#include "Common.h"
#include "Singleton.h"

// interface types for singletons
#if defined(WIN32)
#include "win32/CaptureDX.h"
#include "win32/Timer_win32.h"
#include "win32/Server_win32.h"
#include "win32/WindowManager_win32.h"
#include "win32/KeyboardCommand_win32.h"
#elif defined(LINUX)
#include "linux/CaptureSCL.h"
#include "linux/Timer_linux.h"
#include "linux/Server_linux.h"
#include "linux/WindowManager_linux.h"
#include "linux/KeyboardCommand_linux.h"
#elif defined(OSX)
#endif // platform block

#include "ServerSettings.h"

class CGlobal
{
    CGlobal() = delete;
    CGlobal(CGlobal&) = delete;  
    CGlobal(const CGlobal&) = delete;  
    CGlobal& operator=(CGlobal&) = delete; 
    CGlobal& operator=(const CGlobal&) = delete; 

public:
    static void Init();
    static void Shutdown();

    // NOTE: Dont forget to declare these in the header
    static CSingleton<ITimer> Timer;
    static CSingleton<CServer> Server;
    static CSingleton<ICapture> ScreenCapture;
    static CSingleton<IWindowManager> WindowManager;
    static CSingleton<CServerSettings> ServerSettings;
    static CSingleton<CKeyboardCommand> KeyboardCommand;
};

#endif //_GLOBAL_