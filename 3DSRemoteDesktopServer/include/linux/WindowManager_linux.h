#ifndef _WINDOWMANAGER_LINUX_H_
#define _WINDOWMANAGER_LINUX_H_

#include "../IWindowManager.h"

class IWindowData;
class CWindowManager_linux : public IWindowManager
{
public:

    CWindowManager_linux();
    ~CWindowManager_linux();

    virtual void RefreshWindowList() override;

private: 

};

#endif // _WINDOWMANAGER_LINUX_H_
