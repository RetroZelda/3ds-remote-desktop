#ifndef _CAPTURESCL_
#define _CAPTURESCL_

#include "../ICapture.h"
#include "Threading.h"

#include <ScreenCapture.h>
#include <functional>
#include <vector>
#include <deque>

typedef SL::Screen_Capture::ImageBGRA ImageBGRA;

typedef SL::Screen_Capture::Image ScreenImage;
typedef SL::Screen_Capture::Monitor ScreenMonitor;
typedef SL::Screen_Capture::IScreenCaptureManager ScreenManager;
typedef SL::Screen_Capture::ICaptureConfiguration<SL::Screen_Capture::ScreenCaptureCallback> ScreenConfiguration;

// a screen capture class that wraps screen_capture_light
class CCaptureSCL : public ICapture
{
public:
    CCaptureSCL();
    ~CCaptureSCL();

	virtual void Init(std::weak_ptr<IWindowData> window) override;
	virtual void Shutdown() override;

    virtual void Start() override;
    virtual void Resume() override;
    virtual void Pause() override;
    virtual void Stop() override;

	virtual uint32 GetNextFrame(CaptureData& out_data) override;

    inline bool IsRunning() const override { return m_CaptureManager != nullptr; }
    inline bool IsPaused() const override { return IsRunning() ? m_CaptureManager->isPaused() : true;  }

protected:

    virtual std::vector<ScreenMonitor> OnMonitorsCaptured();
    virtual void OnFrameChanged(const ScreenImage &img, const ScreenMonitor &monitor);
    virtual void OnNewFrame(const ScreenImage &img, const ScreenMonitor &monitor);
private:

    void AddFrame(const ScreenImage &img, const ScreenMonitor &monitor, EFrameType frame_type);
    void ScaleFrame(const byte* src, int32 src_width, int32 src_height, byte* dest, int32 dest_width, int32 dest_height);
    void GetFinalFrame(const ImageRGB565* source_buffer, CaptureData& captured_frame, int32 src_width, int32 src_height) const;

    byte* m_ScaleCache;
    byte* m_ScreenCache;

    int64 m_ScaleCacheSize;
    int64 m_ScreenCacheSize;

    std::mutex m_BufferLock;
    std::deque<CaptureData> m_FrameBuffer;
    std::shared_ptr<IWindowData> m_WindowData;
    std::shared_ptr<ScreenManager> m_CaptureManager;
    std::shared_ptr<ScreenConfiguration> m_CaptureConfiguration;
};

#endif // _CAPTURESCL_