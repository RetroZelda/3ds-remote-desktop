
#ifndef _TIMER_LINUX_H_
#define _TIMER_LINUX_H_

#include "../ITimer.h"
#include "Common.h"

#include <time.h>

class CTimer_linux : public ITimer
{
public:
	CTimer_linux(void);
	~CTimer_linux(void);

	//stop watch functions
	virtual void Start() override;
	virtual void Stop() override;
	virtual void Update() override;
	virtual void Reset() override;

	// modifiers
	virtual void Pause() override;
	virtual void Resume() override;

	// accessors
	virtual double GetElapsedTime() const override;
	virtual double GetDeltaTime() const override;
	virtual int  FPS() const override;

private:
	double		m_dElapsedTime; // stored time
	double		m_dDeltaTime;
	bool		m_bIsRunning;
	timespec	m_StartTick;
	timespec	m_FrameTick;
	
	// for fps
	int32		m_nFrameCount;
	double		m_dFPSTimeStamp;
	int32		m_nFPS;
};
#endif // _TIMER_WIN32_H_