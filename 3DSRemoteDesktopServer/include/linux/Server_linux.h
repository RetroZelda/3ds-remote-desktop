
#ifndef _SERVER_LINUX_H_
#define _SERVER_LINUX_H_

#include "../Server.h"

class CServer_linux : public CServer
{
public:
    virtual bool Init(int16 sPort) override;
    virtual void Shutdown() override;
    virtual int32 GetMyIP() const;

private:
};

#endif // _SERVER_LINUX_H_
