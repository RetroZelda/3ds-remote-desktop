#ifndef _WINDOWDATA_LINUX_H_
#define _WINDOWDATA_LINUX_H_

#include "../IWindowData.h"
#include <ScreenCapture.h>

struct SWindowHandle_linux : public SWindowHandle
{
    SL::Screen_Capture::Window WindowData;
};

class CWindowData_linux : public IWindowData
{
    CWindowData_linux() = delete;
public:
    CWindowData_linux(std::shared_ptr<SWindowHandle> handle);
    CWindowData_linux(SL::Screen_Capture::Window window);
    ~CWindowData_linux();

    // IWindowData
    virtual void UpdateWindowInfo() override;
	virtual void ForceWindowOnTop(bool bForce) override;

private:
};

#endif // _WINDOWDATA_LINUX_H_
