#include "../KeyboardCommand.h"

#include "Threading.h"

#include <linux/input.h>
#include <string>

#ifndef _KEYBOARDCOMMAND_LINUX_H_
#define _KEYBOARDCOMMAND_LINUX_H_

class CServerSettings;
class CKeyboardCommand_linux : public CKeyboardCommand
{
public:

    CKeyboardCommand_linux();
    ~CKeyboardCommand_linux();
    virtual void Init() override;
    virtual void Shutdown() override;
    virtual void SetKeyboardKeyState(int32 key_code, int16 key_state) override;
    virtual int16 GetKeyboardKeyState(int32 key_code) override;

private:
    volatile bool m_IsActive;

    int32 m_KeyboardHandle;
    int16 m_KeyboardState[KEY_CNT];

    std::string m_Name;
    std::mutex  m_KeyMutex;
    std::unique_ptr<std::thread> m_Thread;
    
    void ThreadTick();
};


#endif // _KEYBOARDCOMMAND_LINUX_H_