
#ifndef _KEYBOARDCOMMAND_H_
#define _KEYBOARDCOMMAND_H_

#include "Common.h"
#include "Types.h"

#include <map>

class CKeyboardCommand
{
public:

    CKeyboardCommand();
    virtual ~CKeyboardCommand();
    virtual void Init();
    virtual void Shutdown();
    virtual void SetKeyboardKeyState(int32 key_code, int16 key_state) = 0;
    virtual int16 GetKeyboardKeyState(int32 key_code) = 0;

    void Translate3DSButtonToKeyState(uint32 BTN, int16 key_state);
    void Translate3DSAnalogToKeyState(int16 sX, int16 sY);

private:
    std::map<uint32, int32> m_3DSKeyMap; // <3ds key, keycode>
};


#endif // _KEYBOARDCOMMAND_H_
