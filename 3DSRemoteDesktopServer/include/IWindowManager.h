#ifndef _IWINDOWMANAGER_H_
#define _IWINDOWMANAGER_H_

#include "Common.h"
#include <vector>

class IWindowData;
class IWindowManager
{
public:
    class iterator
    {
        friend IWindowManager;
        IWindowManager* m_pWindowManager;
        std::vector<std::shared_ptr<IWindowData>>::iterator m_pWindowIter;
    protected:
        //iterator();
    public:
        iterator next();
        iterator prev();
        std::weak_ptr<IWindowData> operator->() const;
        std::weak_ptr<IWindowData> operator*() const;
        bool operator!=(iterator other);
    };

    iterator begin();
    iterator end();

    IWindowManager(std::shared_ptr<IWindowData> desktop_window);
    virtual ~IWindowManager();

    virtual void RefreshWindowList() = 0;
    virtual void ClearWindowList();

    std::weak_ptr<IWindowData> GetWindow(int nIndex) const;
    std::weak_ptr<IWindowData> operator[](int nIndex) const;

    uint32 size() const { return m_WindowList.size(); }
    bool IsGenerated() const { return size() > 0;}
    std::weak_ptr<IWindowData> GetDesktopWindow() const { return m_DesktopWindow; }
protected:

    void AddWindow(std::shared_ptr<IWindowData> window_data);

private:

    std::shared_ptr<IWindowData> m_DesktopWindow;
    std::vector<std::shared_ptr<IWindowData>> m_WindowList;
};

#endif // _IWINDOWMANAGER_H_
