#ifndef _WINDOWDATA_WIN32_H_
#define _WINDOWDATA_WIN32_H_

#include "../IWindowData.h"

#include <windef.h>

struct SWindowHandle_win32 : public SWindowHandle
{
    HWND WindowHandle;
};

class CWindowData_win32 : public IWindowData
{
    CWindowData_win32() = delete;
public:
    CWindowData_win32(std::shared_ptr<SWindowHandle> handle);
    CWindowData_win32(HWND handle);
    ~CWindowData_win32();

    // IWindowData
    virtual void UpdateWindowInfo() override;
	virtual void ForceWindowOnTop(bool bForce) override;

private:
};

#endif // _WINDOWDATA_WIN32_H_
