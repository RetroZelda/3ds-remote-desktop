#include "../KeyboardCommand.h"

#include <winuser.h>

#ifndef _KEYBOARDCOMMAND_WIN32_H_
#define _KEYBOARDCOMMAND_WIN32_H_

class CServerSettings;
class CKeyboardCommand_win32 : public CKeyboardCommand
{
public:

    CKeyboardCommand_win32();
    ~CKeyboardCommand_win32();
    virtual void Init() override;
    virtual void Shutdown() override;
    virtual void SetKeyboardKeyState(int32 key_code, int16 key_state) override;
    virtual int16 GetKeyboardKeyState(int32 key_code) override;
};


#endif // _KEYBOARDCOMMAND_WIN32_H_