#ifndef _WINDOWMANAGER_WIN32_H_
#define _WINDOWMANAGER_WIN32_H_

#include "../IWindowManager.h"
#include <windows.h>
#include <stdio.h>

class IWindowData;
class CWindowManager_win32 : public IWindowManager
{
    friend BOOL CALLBACK enumWindowsProc( HWND hWnd, LPARAM lParam);
public:

    CWindowManager_win32();
    ~CWindowManager_win32();

    virtual void RefreshWindowList() override;

private: 

};

#endif // _WINDOWMANAGER_WIN32_H_
