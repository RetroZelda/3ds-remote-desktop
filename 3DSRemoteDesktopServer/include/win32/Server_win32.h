
#ifndef _SERVER_WIN32_H_
#define _SERVER_WIN32_H_

#include "../Server.h"

class CServer_win32 : public CServer
{
public:
    virtual bool Init(int16 sPort) override;
    virtual void Shutdown() override;
    virtual int32 GetMyIP() const;

private:
    WSADATA m_wsa;
};

#endif // _SERVER_WIN32_H_
