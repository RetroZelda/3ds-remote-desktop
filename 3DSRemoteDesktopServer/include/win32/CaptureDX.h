
#ifndef _CAPTUREDX_H_
#define _CAPTUREDX_H_

#include <windows.h>

#include "ServerSettings.h"
#include "../ICapture.h"
#include <deque>

struct IDirect3D9;
struct IDirect3DDevice9;
struct IDirect3DSurface9;

class CWindowData_win32;
class CServer;

// a screen capture class that utilizes win32 and dxd9
class CCaptureDX : public ICapture
{
public:
	CCaptureDX(void);
	~CCaptureDX(void);

	virtual void Init(std::weak_ptr<IWindowData> window) override;
	virtual void Shutdown() override;

    virtual void Start() override;
    virtual void Resume() override;
    virtual void Pause() override;
    virtual void Stop() override;

	virtual uint32 GetNextFrame(CaptureData& out_data) override;

    inline bool IsRunning() const override { return m_IsRunning; }
    inline bool IsPaused() const override { return m_IsRunning && m_IsPaused;  }

	void Temp_Update();

private:
	// server settings
	CServerSettings m_pSettings;

	// to init DirectX
	IDirect3D9* m_pInterface;
	IDirect3DDevice9* m_pDevice;
	CWindowData_win32* m_pWindow;

	bool m_IsRunning = { false };
	bool m_IsPaused = { false };


	uint32 m_unScreenWidth;
	uint32 m_unScreenHeight;
	RECT m_ScreenRect;

	// for capturing the screen;
	IDirect3DSurface9* m_pFinalSurface;
	IDirect3DSurface9* m_pBufferSurface;

	std::deque<CaptureData> m_FrameBuffer;

	// for resizing
	struct PixelColor
	{
		byte A, R, G, B;
	};

	// functions
	uint16 CaptureFrame(CaptureData& out_data);
};

#endif // _CAPTUREDX_H_
