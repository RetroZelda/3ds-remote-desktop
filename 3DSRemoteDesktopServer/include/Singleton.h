#ifndef _SINGLETON_
#define _SINGLETON_

#include "Common.h"

template<typename T>
class CSingleton
{
    friend class CGlobal;
public:

    static const std::unique_ptr<T>& GetInstance() 
    {
        assert(m_Instance != nullptr && "Singleton instance not set");
        return m_Instance; 
    }

    T* operator->() const { return m_Instance.get(); }
    T* operator*() const { return m_Instance.get(); }
protected:

    template<typename T1>
    static void Create() 
    {
        assert(m_Instance == nullptr && "Singleton instance already set");
        m_Instance.reset(dynamic_cast<T*>(new T1()));
    }

    static void Destroy() 
    {
        m_Instance.reset();
    }
private:

    static std::unique_ptr<T> m_Instance;
};

template<typename T>
std::unique_ptr<T> CSingleton<T>::m_Instance;

#endif //_SINGLETON_