#ifndef _IWINDOWDATA_
#define _IWINDOWDATA_

#include "Types.h"
#include "Common.h"

struct SWindowHandle
{

};

class IWindowData
{
public:

    IWindowData(std::shared_ptr<SWindowHandle> handle) : m_WindowHandle(handle) {}
    ~IWindowData() {};

    virtual void UpdateWindowInfo() = 0;
	virtual void ForceWindowOnTop(bool bForce) = 0;

    uint32 GetWidth() const { return m_WindowWidth;}
    uint32 GetHeight() const { return m_WindowHeight;}
    const Rectangle64& GetWindowRect() const { return m_ScreenRect;}
    const std::string& GetDisplayText() const { return m_WindowText;}
    std::weak_ptr<SWindowHandle> GetHandle() const { return m_WindowHandle;}


    template<typename T = SWindowHandle>
    std::weak_ptr<T> GetHandle() const { return std::static_pointer_cast<T>(m_WindowHandle);}

protected:

    void SetWidth(uint32 width) { m_WindowWidth = width;}
    void SetHeight(uint32 height) { m_WindowHeight = height;}
    void SetWindowRect(const Rectangle64& rect) { m_ScreenRect = rect;}
    void SetDisplayText(const char* text) { m_WindowText = text;}

    template<typename T = SWindowHandle>
    std::shared_ptr<T> GetSharedHandle() const { return std::static_pointer_cast<T>(m_WindowHandle);}

    std::string m_WindowText;
private:

	uint32 m_WindowWidth = { 0 };
	uint32 m_WindowHeight = { 0 };
	Rectangle64 m_ScreenRect;
    std::shared_ptr<SWindowHandle> m_WindowHandle;
};

#endif // _IWINDOWDATA_