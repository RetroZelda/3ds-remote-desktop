#ifndef _COMMON_
#define _COMMON_

#include <assert.h>
#include <string>
#include <memory>

#include "Types.h"

#if defined(WIN32)

#include <windef.h>

typedef RECT Rectangle64;

#elif defined(LINUX)

typedef struct {
  int64 left;
  int64 top;
  int64 right;
  int64 bottom;
} Rectangle64;

#elif defined(OSX)

#endif // platform block


#endif // _COMMON_