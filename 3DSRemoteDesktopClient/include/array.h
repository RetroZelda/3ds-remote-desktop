
#ifndef _ARRAY_H_
#define _ARRAY_H_

#include "Types.h"

typedef byte* Array;

// gives you an empty array
// in:
//      capacity_hint - gives a hint for the start capacity
//      enforce_capacity - if true, wont add elements beyond the capacity
Array array_create(uint32 capacity_hint, bool enforce_capacity);

// destroy the array
// NOTE: you need to handle anything the array is holding.
void array_destroy(Array array);

// clear the array but keep the capacity
bool array_clear(Array array);

// make the capacity fit the current size
bool array_make_fit(Array array);

// return true(1) if data was added, else false(0)
bool array_push_front(Array array, byte* data);
bool array_push_back(Array array, byte* data);
bool array_insert(Array array, uint32 index, byte* data);

// returns the top data and removes it from the array
byte* array_pop_front(Array array);
byte* array_pop_back(Array array);
byte* array_remove(Array array, uint32 index);

// returns the top data
byte* array_get(const Array array, uint32 index);

uint32 array_size(Array array);
uint32 array_capacity(Array array);


#endif // _ARRAY_H_
