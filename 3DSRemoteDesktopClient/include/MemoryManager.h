#ifndef _MEMORYMANAGER_H_
#define _MEMORYMANAGER_H_

#include "Types.h"

#define MEMORY_ENABLE_MAPPABLE 0
#define MEMORY_ENABLE_LINEAR 1
#define MEMORY_ENABLE_VRAM 0

typedef enum
{
    #if MEMORY_ENABLE_MAPPABLE
    MEMORY_MAPPABLE,
    #endif 

    #if MEMORY_ENABLE_LINEAR
    MEMORY_LINEAR,
    #endif 

    #if MEMORY_ENABLE_VRAM
    MEMORY_VRAM,
    #endif 
    //MEMORY_HEAP
} MemoryPoolType;

#define DEFAULT_POOL MEMORY_LINEAR

void Memory_Init();
void Memory_Shutdown();

byte* Memory_Pool_Allocate(MemoryPoolType pool, uint32 requested_size);
byte* Memory_Pool_Reallocate(MemoryPoolType pool, byte* data, uint32 requested_size);
byte* Memory_Pool_Callocate(MemoryPoolType pool, uint32 num, uint32 requested_size);
void Memory_Pool_Free(MemoryPoolType pool, byte* data);

uint32 Memory_Pool_Remaining(MemoryPoolType pool);
uint32 Memory_Pool_Capacity(MemoryPoolType pool);
float Memory_Pool_Percent(MemoryPoolType pool);

void Memory_Print();

static inline byte* Memory_Allocate(uint32 requested_size)
{
    return Memory_Pool_Allocate(DEFAULT_POOL, requested_size);
}

static inline byte* Memory_Reallocate(byte* data, uint32 requested_size)
{
    return Memory_Pool_Reallocate(DEFAULT_POOL, data, requested_size);
}

static inline byte* Memory_Callocate(uint32 num, uint32 requested_size)
{
    return Memory_Pool_Callocate(DEFAULT_POOL, num, requested_size);
}

static inline void Memory_Free(byte* data)
{
    Memory_Pool_Free(DEFAULT_POOL, data);
}

#endif // _MEMORYMANAGER_H_