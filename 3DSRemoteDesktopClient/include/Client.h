
#ifndef _CLIENT_H_
#define _CLIENT_H_

#include "Packet.h"

typedef void (*PacketHandlerFunc)(const Packet* packet);

void Client_Init();
void Client_Shutdown();
void Client_Connect(char* server_ip, uint16 port);
void Client_Update();
void Client_Disconnect();

void Client_SendData(PacketType packet_type, uint32 data_size, byte* data);
void Client_AddListener(PacketType packet_type, PacketHandlerFunc func);
void Client_RemoveListener(PacketType packet_type, PacketHandlerFunc func);

int32 Client_IsConnected();

#endif // _CLIENT_H_
