
#ifndef _INPUTMANAGER_H_
#define _INPUTMANAGER_H_

#include <3ds.h>


// TODO: Handle 3DS button, touch, motion, and microphone input
// NOTE: 3DS input info in all found in ctrulib's 3ds/services/hid.h
typedef enum _3DSButtons_T{
               BTN_A        = KEY_A,
               BTN_B        = KEY_B,
               BTN_X        = KEY_X,
               BTN_Y        = KEY_Y,
               BTN_L        = KEY_L,
               BTN_R        = KEY_R,
               BTN_D_UP     = KEY_DUP,
               BTN_D_DOWN   = KEY_DDOWN,
               BTN_D_LEFT   = KEY_DLEFT,
               BTN_D_RIGHT  = KEY_DRIGHT,
               BTN_START    = KEY_START,
               BTN_SELECT   = KEY_SELECT} _3DSButtons;



void Input_Init();
void Input_Update();

int KeyDown(_3DSButtons _BTN);
int KeyUp(_3DSButtons _BTN);
int KeyPressed(_3DSButtons _BTN);
int KeyReleased(_3DSButtons _BTN);

circlePosition GetCirclepadState();


#endif // CINPUTMANAGER_H_
