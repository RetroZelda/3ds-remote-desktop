
#ifndef _EXTENDEDCONSOLE_H_
#define _EXTENDEDCONSOLE_H_

#include <stdlib.h>
#include <stdio.h>
#include <3ds.h>

void OpenExtendedConsole(gfxScreen_t DefaultScreen);

void PrintToScreen(gfxScreen_t TargetScreen, u16 X, u16 Y, char* szString, ...);

#endif
