#ifndef _SCREENDISPLAY_H_
#define _SCREENDISPLAY_H_

#include "Packet.h"

void Screen_Init();
void Screen_Clear();
void Screen_Draw();
void Screen_Shutdown();

void Screen_AddImage(const ScreenDescriptor* image_data, uint32 compressed_size);

#endif // _SCREENDISPLAY_H_