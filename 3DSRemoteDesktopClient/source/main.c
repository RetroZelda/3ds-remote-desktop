

#include "ExtendedConsole.h"
#include "MemoryManager.h"
#include "ScreenDisplay.h"
#include "InputManager.h"
#include "Packet.h"
#include "Client.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
#include <3ds.h>
#include <lz4.h>

#define HANDLE_BUTTON(BTN)\
    temp = (uint32)(BTN);\
    if(KeyPressed(BTN))\
        Client_SendData(PACKET_INPUT_PRESSED, sizeof(uint32), (byte*)&temp);\
    else if(KeyReleased(BTN))\
        Client_SendData(PACKET_INPUT_RELEASED, sizeof(uint32), (byte*)&temp);

circlePosition _PrevCirclePad;
void DoInput()
{
    uint32 temp = 0;
    HANDLE_BUTTON(BTN_A);
    HANDLE_BUTTON(BTN_B);
    HANDLE_BUTTON(BTN_X);
    HANDLE_BUTTON(BTN_Y);
    HANDLE_BUTTON(BTN_L);
    HANDLE_BUTTON(BTN_R);
    HANDLE_BUTTON(BTN_D_UP);
    HANDLE_BUTTON(BTN_D_DOWN);
    HANDLE_BUTTON(BTN_D_LEFT);
    HANDLE_BUTTON(BTN_D_RIGHT);
    HANDLE_BUTTON(BTN_START);
    HANDLE_BUTTON(BTN_SELECT);

    circlePosition circlePad = GetCirclepadState();
    if(memcmp(&_PrevCirclePad, &circlePad, sizeof(circlePosition)) != 0)
    {
        Client_SendData(PACKET_INPUT_CIRCLEPAD, sizeof(circlePosition), (void*)&circlePad);
        memcpy(&_PrevCirclePad, &circlePad, sizeof(circlePosition));
    }
}

char szIP[15];
short sPort = 8888;

int nCurInputIndex = 12; // starts at beginning of last IP segment
void IPInput()
{
    if(KeyPressed(BTN_D_LEFT))
        nCurInputIndex--;
    if(KeyPressed(BTN_D_RIGHT))
        nCurInputIndex++;
    if(KeyPressed(BTN_L))
        sPort--;
    if(KeyPressed(BTN_R))
        sPort++;

    if(nCurInputIndex < 0)
        nCurInputIndex = 0;
    if(nCurInputIndex > 14)
        nCurInputIndex = 14;


    int curChar = (int)szIP[nCurInputIndex];
    if(curChar != '.')
    {
        if(KeyPressed(BTN_D_UP))
        {
            curChar++;
            if(curChar > (int)'9')
                curChar = (int)'9';
        }
        if(KeyPressed(BTN_D_DOWN))
        {
            curChar--;
            if(curChar < (int)'0')
                curChar = (int)'0';
        }
        szIP[nCurInputIndex] = (char)curChar;
    }

    PrintToScreen(GFX_TOP, 16, 11, "Connect to: %s:%d", szIP, sPort);
    PrintToScreen(GFX_TOP, 17, 23 + (nCurInputIndex), "^");


    if(KeyPressed(BTN_A))
    {
        Client_Connect(szIP, sPort);
    }
}

void PacketScreenData(const Packet* packet)
{
    if(packet->_PacketID == PACKET_SCREEN)
    {
        // We recieved the screen
        ScreenPacketData* pScreenShot = (ScreenPacketData*)packet->_PacketData;
        ScreenDescriptor* screen_data = (ScreenDescriptor*)Memory_Allocate(sizeof(ScreenDescriptor) + pScreenShot->CompressedSize);
        if(screen_data)
        {
            memcpy(screen_data, &pScreenShot->Data, sizeof(ScreenDescriptor) + pScreenShot->CompressedSize);
            Screen_AddImage(screen_data, pScreenShot->CompressedSize);
        }
    }
}

int main()
{
    memcpy(szIP, "192.168.000.000", 15);
    szIP[15] = '\0';

	// Initialize services
	srvInit();
	aptInit();
	hidInit();
	gfxInit(GSP_RGB565_OES, GSP_RGB565_OES, false);
	//gfxSet3D(true); // uncomment if using stereoscopic 3D

    Memory_Init();

    // start console
    OpenExtendedConsole(GFX_BOTTOM); // NOTE: Built-in console forces GSP_RGB565_OES

    printf("LZ4 Library version = %ld\n", (int32)LZ4_versionNumber());

    Input_Init();
    Client_Init();
    Screen_Init();

    Client_AddListener(PACKET_SCREEN, &PacketScreenData);

	// Main loop
	while (aptMainLoop())
	{
		gspWaitForVBlank();
        Input_Update();
        Client_Update();

		if (KeyPressed(BTN_START))
			break; // break in order to return to hbmenu

        if(Client_IsConnected())
        {
            DoInput();
        }

        if(!Client_IsConnected())
        {
            Screen_Clear();
            IPInput();
        }

        Screen_Draw();
        Memory_Print();

		// Flush and swap framebuffers
		gfxFlushBuffers();
		gfxSwapBuffers();
	}

    Client_RemoveListener(PACKET_SCREEN, &PacketScreenData);
    Client_Disconnect();
    Client_Shutdown();
    Screen_Shutdown();
    Memory_Shutdown();

	// Exit services
	gfxExit();
	hidExit();
	aptExit();
	srvExit();
	return 0;
}

/*
// NOTE: this is code used to test memory manager with linear CTR ram
#include "queue.h"
int main()
{
	srvInit();
	aptInit();
	hidInit();
	gfxInit(GSP_RGB565_OES, GSP_RGB565_OES, false);
    
    do
    {
        Input_Update();
        if (KeyPressed(BTN_START))
            break;
    } while (true);

    Memory_Init();

    OpenExtendedConsole(GFX_TOP); 
    Input_Init();

    Queue queue = queue_create();
	while (aptMainLoop())
	{        
        Input_Update();
        if (KeyPressed(BTN_START))
            break;

        if (KeyDown(BTN_A))
        {
            queue_push_back(queue, Memory_Allocate(MEMORY_LINEAR, 64));
        }
        if (KeyDown(BTN_B))
        {
            Memory_Free(MEMORY_LINEAR, queue_pop_front(queue));
        }       

        Memory_Print();

        svcSleepThread(1000000000/4);

		gspWaitForVBlank();
        byte* topFrameBuffer = gfxGetFramebuffer(GFX_TOP, GFX_LEFT, NULL, NULL);
        memset(topFrameBuffer, 0, 240*400*2);
    }


    Memory_Shutdown();
	gfxExit();
	hidExit();
	aptExit();
	srvExit();
}
*/

/*
// NOTE: used to test memory manager usage
#include "MemoryManager.h"
#include "array.h"
#include <stdio.h>
#include <string.h>
int main(int32 argc, char* argv[])
{
	srvInit();
	aptInit();
	hidInit();
	gfxInit(GSP_RGB565_OES, GSP_RGB565_OES, false);
    

    do
    {
        Input_Update();
        if (KeyPressed(BTN_START))
            break;
    } while (true);

    Memory_Init();
    Array storage = array_create(64, false);

    uint8 count = 10;
    while(count-- > 0)
    {
        array_push_back(storage, Memory_Allocate(16));
    }

    while(array_size(storage) > 0)
    {
        Memory_Free(array_pop_back(storage));
    }

    {
        // Test shrink realloc
        byte* mem = Memory_Allocate(64);
        memset(mem, 0xAA, 64);
        byte* new_mem = Memory_Reallocate(mem, 16);
        memset(new_mem, 0xBB, 16);
        Memory_Free(new_mem);
    }

    {
        // test merge next realloc
        byte* mem0 = Memory_Allocate(16);
        byte* mem1 = Memory_Allocate(16);
        byte* mem2 = Memory_Allocate(16);
        memset(mem0, 0xAA, 16);
        memset(mem1, 0xAA, 16);
        memset(mem2, 0xAA, 16);
        Memory_Free(mem1);
        Memory_Free(mem2);
        byte* new_mem = Memory_Reallocate(mem0, 32);
        memset(new_mem, 0xBB, 32);
        Memory_Free(new_mem);
    }

    {
        // test merge prev realloc 
        byte* mem0 = Memory_Allocate(16);
        byte* mem1 = Memory_Allocate(16);
        byte* mem2 = Memory_Allocate(16);
        memset(mem0, 0xAA, 16);
        memset(mem1, 0xAA, 16);
        memset(mem2, 0xAA, 16);
        Memory_Free(mem0);
        Memory_Free(mem1);
        byte* new_mem = Memory_Reallocate(mem2, 32);
        memset(new_mem, 0xBB, 32);
        Memory_Free(new_mem);
    }

    while(array_size(storage) > 0)
    {
        Memory_Free(array_pop_front(storage));
    }
    Memory_Shutdown();
	gfxExit();
	hidExit();
	aptExit();
	srvExit();
}
*/