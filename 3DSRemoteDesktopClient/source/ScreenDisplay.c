
#include "ExtendedConsole.h"
#include "ScreenDisplay.h"
#include "MemoryManager.h"
#include "Packet.h"
#include "queue.h"

#include <string.h>
#include <stdio.h>
#include <3ds.h>
#include <lz4.h>

#define QUEUE_SIZE 10

typedef struct _CompressedScreenData
{
    const ScreenDescriptor* image_data;
    uint32 compressed_size;
} CompressedScreenData;

Queue _ScreenQueue = NULL;
byte* _DecompressCache;

void Screen_Init()
{
    _ScreenQueue = queue_create();
    _DecompressCache = Memory_Allocate(400*240*2);
}

void Screen_Shutdown()
{
    while(queue_size(_ScreenQueue) > 0)
    {
        CompressedScreenData* next_screen = (CompressedScreenData*)queue_pop_front(_ScreenQueue);
        Memory_Free((byte*)next_screen->image_data);
        Memory_Free((byte*)next_screen);
    }
    queue_destroy(_ScreenQueue);
    Memory_Free((byte*)_DecompressCache);
}

void Screen_Clear()
{
    byte* topFrameBuffer = gfxGetFramebuffer(GFX_TOP, GFX_LEFT, NULL, NULL);
    memset(topFrameBuffer, 0, 240*400*3);
}

void Screen_Draw()
{
    uint16* topFrameBuffer = (uint16*)gfxGetFramebuffer(GFX_TOP, GFX_LEFT, NULL, NULL);
    CompressedScreenData* next_screen = (CompressedScreenData*)queue_pop_front(_ScreenQueue);
    if(next_screen)
    {
        // Decompress the screen data
        const ScreenDescriptor* image_data = next_screen->image_data;
        int32 decompress_size = LZ4_decompress_safe((char*)image_data->PixelData, (char*)_DecompressCache,
                                                    next_screen->compressed_size, image_data->Width * image_data->Height * 2);

        if(decompress_size < 0)
        {
            printf("Problem decompressing screen(%ld)\n", decompress_size);
        }
        else
        {
            uint16 PosX = image_data->X;
            uint16 PosY = image_data->Y;
            uint16 Width = image_data->Width;
            uint16 Height = image_data->Height;

            // lets fill the screen based on the rect
            const uint16* startsrc = (uint16*)_DecompressCache;
            for (uint16 x = PosX; x < PosX + Width; x++) 
            {
                uint16* startdst = topFrameBuffer + (x * 240) + (240 - PosY - Height);
                memcpy(startdst, startsrc, sizeof(uint16) * Height);
                startsrc += Height;
            }

            PrintToScreen(GFX_TOP, 20, 0, "Screen Compression - %ld%%", (int32)(((float)next_screen->compressed_size / (float)(image_data->Width * image_data->Height * 2)) * 100.0f));
        }
        Memory_Free((byte*)next_screen->image_data);
        Memory_Free((byte*)next_screen);
    }
}

void Screen_AddImage(const ScreenDescriptor* image_data, uint32 compressed_size)
{
    if(queue_size(_ScreenQueue) >= QUEUE_SIZE)
    {
        Memory_Free((byte*)image_data);
        return;
    }

    CompressedScreenData* new_screen = (CompressedScreenData*)Memory_Allocate(sizeof(CompressedScreenData));
    if(new_screen)
    {
        new_screen->image_data = image_data;
        new_screen->compressed_size = compressed_size; 
        queue_push_back(_ScreenQueue, (byte*)new_screen);
    }
    else
    {
        Memory_Free((byte*)image_data);
    }
}

