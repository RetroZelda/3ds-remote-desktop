/*
Copyright (c) 2015, Erick<RetroZelda>Folckemer
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "Client.h"

#include "ExtendedConsole.h"
#include "InputManager.h"
#include "RetroNetwork.h"
#include "Types.h"
#include "queue.h"
#include "array.h"

#include <string.h>
#include <stdlib.h>
#include <malloc.h>

/*
#define PrintToScreen(...)
#define printf(...)
*/
u32 _IP;
struct sockaddr_in _ServerAddr;
int32 _ClientSocket;
bool _bHasServerInfo = false;
char* _szServerIP;
int16 _sServerPort;
bool _bConnected = false;

Queue _PacketQueue = NULL;
Array _SubscriberArray[PACKET_TOTAL];

typedef enum {CONNECTED, DISCONNECTED} ConnectionStatus_t;
ConnectionStatus_t _Status;

void Dispatch(Packet* packet)
{
    // printf("[recv][size: %ld][type: %ld]\n", packet->_PacketSize, packet->_PacketID);

    for(int32 subscriber_index = 0; subscriber_index < array_size(_SubscriberArray[packet->_PacketID]); ++subscriber_index)
    {
        PacketHandlerFunc subscriber_func = (PacketHandlerFunc)array_get(_SubscriberArray[packet->_PacketID], subscriber_index);
        (*subscriber_func)(packet);
    }

    switch(packet->_PacketID)
    {
    case PACKET_SCREEN:
        Client_SendData(PACKET_SCREENRETRIEVED, 0, NULL);
        break;
    case PACKET_HANDSHAKEREQUEST:
        Client_SendData(PACKET_HANDSHAKERESPONSE, 0, NULL);
        break;
    case PACKET_MESSAGE:
        printf("Server: %s\n", packet->_PacketData);
        break;
    };

}

int32 nPrevRecvError = 0;
void RecievePacketData()
{
    int32 _errno;
    Packet* pRecvPacket;
    int32 ret = RNRecieveData(_ClientSocket, &pRecvPacket, &_errno);
    if(ret > 0)
    {
        Dispatch(pRecvPacket);
        free(pRecvPacket);
    }
    else if(ret == 0)
    {
        printf("We were disconnected from the server.\n");
        _Status = DISCONNECTED;
        closesocket(_ClientSocket);
    }
    else if(ret == -2)
    {
        PrintToScreen(GFX_TOP, 0, 0, "recv() failed(%ld). Unable to Allocate space!");
    }
    else
    {
        #define PRINT_CRAP(CASE, REPORT) \
        case CASE:\
        case -CASE:\
        PrintToScreen(GFX_TOP, 0, 0, "[ERR %ld] %s", ret, REPORT);\
        if(nPrevRecvError != _errno){\
        printf("[ERR %ld] %s\n", ret, REPORT);\
        nPrevRecvError = _errno;\
        }\
        break;
        switch(_errno)
        {
            // PRINT_CRAP(EAGAIN, "EAGAIN: Trying again..."); // NOTE: Same code as EWOULDBLOCK
            PRINT_CRAP(EBADF, "The socket argument is not a valid file descriptor.");
            PRINT_CRAP(ECONNRESET, "A connection was forcibly closed by a peer.");
            PRINT_CRAP(EINTR, "The recv() function was interrupted by a signal that was caught, before any data was available.");
            PRINT_CRAP(EINVAL, "The MSG_OOB flag is set and no out-of-band data is available.");
            PRINT_CRAP(ENOTCONN, "A receive is attempted on a connection-mode socket that is not connected.");
            PRINT_CRAP(ENOTSOCK, "The socket argument does not refer to a socket.");
            PRINT_CRAP(EOPNOTSUPP, "The specified flags are not supported for this socket type or protocol.");
            PRINT_CRAP(ETIMEDOUT, "The connection timed out during connection establishment, or due to a transmission timeout on active connection.");
            PRINT_CRAP(EIO, "An I/O error occurred while reading from or writing to the file system.");
            PRINT_CRAP(ENOBUFS, "Insufficient resources were available in the system to perform the operation.");
            PRINT_CRAP(ENOMEM, "Insufficient memory was available to fulfill the request.");
        case EWOULDBLOCK:
            break;
        default:
            PrintToScreen(GFX_TOP, 0, 0, "recv() failed(%ld) with code: 0x%lx(%ld)", ret, _errno, _errno);
            break;
        }
        #undef PRINT_CRAP
    }
}

void FlushSendQueue()
{
    Packet* pToSend = (Packet*)queue_pop_front(_PacketQueue);
    int32 error = 0;
    while(pToSend != NULL)
    {
        int32 nPacketLength = sizeof(Packet) + pToSend->_PacketSize;

        // printf("[send][size: %ld][type: %ld]", nPacketLength, pToSend->_PacketID);
        int32 nSendAmmount = send(_ClientSocket, pToSend, nPacketLength, 0);
        if(nSendAmmount < 0)
        {
            error = GETERRNO();
            printf("Problem contacting server. Error code: 0X%lX(%ld)", error, error);
        }

        // release and refresh
        free(pToSend);
        pToSend = (Packet*)queue_pop_front(_PacketQueue);
    }
}

void Client_Init()
{
    _ClientSocket = RNBADSOCKET;
    _Status = DISCONNECTED;
    _bHasServerInfo = false;

    printf("Initializing SOC.\n");
    socInit((u32*)memalign(0x1000, 0x100000), 0x100000);

    // create the packet queue
    _PacketQueue = queue_create();

    for(uint8 packet_id = 0; packet_id < PACKET_TOTAL; ++packet_id)
        _SubscriberArray[packet_id] = array_create(2, false);

    // dont clsoe process when error occurs or something
    // signal(SIGPIPE, SIG_IGN);

/*
    _IP = (u32)gethostid();
    printf("IP: %ld\n", _IP);
*/
}

void Client_Shutdown()
{
    if(_ClientSocket != RNBADSOCKET)
    {
        closesocket(_ClientSocket);
    }

    for(uint8 packet_id = 0; packet_id < PACKET_TOTAL; ++packet_id)
        array_destroy(_SubscriberArray[packet_id]);
    queue_destroy(_PacketQueue);
	socExit();
}

void Client_Connect(char* server_ip, uint16 port)
{
    // close socket if exists
    if(_ClientSocket != RNBADSOCKET || _bHasServerInfo)
    {
        closesocket(_ClientSocket);
    }

    // open a new socket
    printf("Opening Socket.\n");
    _ClientSocket = socket(AF_INET, SOCK_STREAM, 0);
    if(_ClientSocket == RNBADSOCKET)
    {
        printf("Unable to create socket.\n");
        return;
    }

    // stop blocking
    /* NOTE: New method.  test before using
    int32 result = fcntl(_ClientSocket, F_GETFL, 0);
    if(result != RNBADSOCKET)
    {
        result = fcntl(_ClientSocket, F_SETFL, result | O_NONBLOCK);
        if(result == RNBADSOCKET)
        {
            int32 error = GETERRNO();
            printf("fcntl F_SETFL call failed.  err: 0x%lx (%ld).\n", error, error);
        }
    }
    else
    {
        int32 error = GETERRNO();
        printf("fcntl F_GETFL call failed.  err: 0x%lx (%ld).\n", error, error);
    }
    */

    // ensure we are freed
    if(_szServerIP != NULL)
    {
        free(_szServerIP);
    }

    // copy over new server info
    _sServerPort = port;
    _szServerIP = malloc(strlen(server_ip));
    strcpy(_szServerIP, server_ip);
    printf("Set Server: %s:%d\n", _szServerIP, _sServerPort);

    // set the server info
    _ServerAddr.sin_family = AF_INET;
    _ServerAddr.sin_port = htons(_sServerPort);
    _ServerAddr.sin_addr.s_addr = inet_addr(server_ip);

    _bHasServerInfo = true;
}

int32 nPrevConnectError = 0;
void Client_Update()
{
    switch(_Status)
    {
    case CONNECTED:
        // send all data in the packet queue
        //FlushSendQueue();

        // recieve from server
        RecievePacketData();

        break;
    case DISCONNECTED:
        if(_bHasServerInfo)
        {
            printf("Connecting To Server at %s:%d\n", _szServerIP, _sServerPort);
            // connect to the server
            int32 ret = connect(_ClientSocket, (struct sockaddr *)&_ServerAddr, sizeof(_ServerAddr));
            if(ret >= 0)
            {
                printf("Connection to server successful!\n");
                Client_SendData(PACKET_SCREENRETRIEVED, 0, NULL);
                _Status = CONNECTED;
            }
            else
            {
                #define PRINT_CRAP(CASE, REPORT) \
                case CASE:\
                PrintToScreen(GFX_TOP, 1, 5, "[ERR %ld] %s", ret, REPORT);\
                if(nPrevConnectError != error){\
                printf("[ERR %ld] %s\n", ret, REPORT);\
                nPrevConnectError = error;\
                }\
                break;
                uint32 error = GETERRNO();
                switch(-error)
                {
                PRINT_CRAP(EADDRNOTAVAIL, "The specified address is not available from the local machine.")
                PRINT_CRAP(EAFNOSUPPORT, "The specified address is not a valid address for the address family of the specified socket.")
                PRINT_CRAP(EALREADY, "A connection request is already in progress for the specified socket.")
                PRINT_CRAP(EBADF, "The socket argument is not a valid file descriptor.")
                PRINT_CRAP(ECONNREFUSED, "The target address was not listening for connections or refused the connection request.")
                PRINT_CRAP(EINPROGRESS, "O_NONBLOCK is set for the file descriptor for the socket and the connection cannot be immediately established; the connection shall be established asynchronously.")
                PRINT_CRAP(EINTR, "The attempt to establish a connection was interrupted by delivery of a signal that was caught; the connection shall be established asynchronously.")
                //PRINT_CRAP(EISCONN, "The specified socket is connection-mode and is already connected.")
                PRINT_CRAP(ENETUNREACH, "No route to the network is present.")
                PRINT_CRAP(ENOTSOCK, "The socket argument does not refer to a socket.")
                PRINT_CRAP(EPROTOTYPE, "The specified address has a different type than the socket bound to the specified peer address.")
                PRINT_CRAP(ETIMEDOUT, "The attempt to connect timed out before a connection was made.")
                PRINT_CRAP(EACCES, "Search permission is denied for a component of the path prefix; or write access to the named socket is denied.")
                PRINT_CRAP(EADDRINUSE, "Attempt to establish a connection that uses addresses that are already in use.")
                PRINT_CRAP(ECONNRESET, "Remote host reset the connection request.")
                PRINT_CRAP(EHOSTUNREACH, "The destination host cannot be reached (probably because the host is down or a remote router cannot reach it).")
                PRINT_CRAP(EINVAL, "The address_len argument is not a valid length for the address family; or invalid address family in the sockaddr structure.")
                PRINT_CRAP(ELOOP, "More than {SYMLOOP_MAX} symbolic links were encountered during resolution of the pathname in address.")
                PRINT_CRAP(ENAMETOOLONG, "Pathname resolution of a symbolic link produced an intermediate result whose length exceeds {PATH_MAX}.")
                PRINT_CRAP(ENETDOWN, "The local network interface used to reach the destination is down.")
                PRINT_CRAP(ENOBUFS, "No buffer space is available.")
                PRINT_CRAP(EOPNOTSUPP, "The socket is listening and cannot be connected.")
                case EISCONN:
                    printf("[ERR: EISCONN]Connection to server successful?\n");
                    _Status = CONNECTED;
                    break;
                default:
                    printf("connect() failed(ret %ld) with code: 0x%lx(%ld)\n", ret, error, error);
                    break;
                }
                #undef PRINT_CRAP
                //printf("Connection to server failed!\n");
                // TODO: Handle ewouldblock and other stuff needed for non-blocking sockets

                Client_Disconnect();
            }

        }
        else
        {
            PrintToScreen(GFX_TOP, 1, 1, "Waiting for Server Info");
        }
        break;
        default:
            PrintToScreen(GFX_TOP, 1, 1, "What?");
        break;
    };
}

void Client_Disconnect()
{
    shutdown(_ClientSocket, 0);
    closesocket(_ClientSocket);
    _Status = DISCONNECTED;
    _bHasServerInfo = false;

    // clear send queue
    Packet* remaining_packet = (Packet*)queue_pop_front(_PacketQueue);
    while(remaining_packet != NULL)
    {
        free(remaining_packet);
        remaining_packet = (Packet*)queue_pop_front(_PacketQueue);
    }

    // ensure we are freed
    if(_szServerIP != NULL)
    {
        free(_szServerIP);
    }
}

void Client_SendData(PacketType packet_type, uint32 data_size, byte* data)
{
    // printf("[send][size: %ld][type: %ld]\n", data_size, packet_type);
    int32 nSendAmmount = RNSendData(_ClientSocket, (uint32)packet_type, data_size, data);
    if(nSendAmmount < 0)
    {
        int32 error = GETERRNO();
        printf("Problem contacting server. Error code: 0x%lx(%ld)\n", error, error);
        Client_Disconnect();
    }
}

void Client_AddListener(PacketType packet_type, PacketHandlerFunc func)
{
    array_push_back(_SubscriberArray[packet_type], (byte*)func);
}

void Client_RemoveListener(PacketType packet_type, PacketHandlerFunc func)
{
    for(int32 subscriber_index = 0; subscriber_index < array_size(_SubscriberArray[packet_type]); ++subscriber_index)
    {
        PacketHandlerFunc subscriber_func = (PacketHandlerFunc)array_get(_SubscriberArray[packet_type], subscriber_index);
        if(func == subscriber_func)
        {
            array_remove(_SubscriberArray[packet_type], subscriber_index);
            break;
        }
    }
}

int32 Client_IsConnected()
{
    if(_Status == CONNECTED)
        return 1;
    return 0;
}








