
#include "ExtendedConsole.h"

#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <string.h>

#define CONSOLE_STRING_BUFFER_SIZE 64

gfxScreen_t _Screen;

PrintConsole* _TopConsole;
PrintConsole* _BottomConsole;
const char* _szMoveCursor = "\x1b[%d;%dH";

PrintConsole TopConsoleSettings;

PrintConsole BottomConsoleSettings;


void OpenExtendedConsole(gfxScreen_t DefaultScreen)
{
    PrintConsole* def = consoleGetDefault();
    memcpy(&TopConsoleSettings, def, sizeof(PrintConsole));
    memcpy(&BottomConsoleSettings, def, sizeof(PrintConsole));

    _TopConsole = consoleInit(GFX_TOP, &TopConsoleSettings);
    _BottomConsole = consoleInit(GFX_BOTTOM, &BottomConsoleSettings);
    _Screen = DefaultScreen;

    switch(DefaultScreen)
    {
    case GFX_TOP:
        consoleSelect(_TopConsole);
        break;
    case GFX_BOTTOM:
        consoleSelect(_BottomConsole);
        break;
    }
}

// void PrintToScreen(gfxScreen_t TargetScreen, u16 X, u16 Y, char* szString, ...){}

void PrintToScreen(gfxScreen_t TargetScreen, u16 X, u16 Y, char* szString, ...)
{
    PrintConsole* _Console;
    PrintConsole* _RetConsole;
    switch(TargetScreen)
    {
    case GFX_TOP:
        _Console = _TopConsole;
        _RetConsole = consoleSelect(_TopConsole);
        break;
    case GFX_BOTTOM:
        _Console = _BottomConsole;
        _RetConsole = consoleSelect(_BottomConsole);
        break;
    }

    int retX = _Console->cursorX;
    int retY = _Console->cursorY;
    _Console->cursorX = Y;
    _Console->cursorY = X;

    // print it
    va_list arguments;
    va_start ( arguments, szString );

    vprintf(szString, arguments);

    va_end ( arguments );


    _Console->cursorX = retX;
    _Console->cursorY = retY;

    consoleSelect(_RetConsole);
}
