
#include "InputManager.h"
#include <string.h>

#define INPUT_PRESSED 1
#define INPUT_RELEASED 0
#define INPUT_STATE char

u32 m_Cur3DSButtonState;
u32 m_Prev3DSButtonState;

circlePosition m_CirclepadPosition;

void Poll3DSButtons()
{
    hidScanInput();
    hidCircleRead(&m_CirclepadPosition);
    m_Prev3DSButtonState = m_Cur3DSButtonState;
    m_Cur3DSButtonState = hidKeysHeld();
}

// TODO: Init 3DS stuff
void Input_Init()
{
    memset(&m_CirclepadPosition, 0, sizeof(circlePosition));
    Poll3DSButtons();
}

void Input_Update()
{
    Poll3DSButtons();
}

INPUT_STATE CurBtnStatus(_3DSButtons _BTN)
{
    return (m_Cur3DSButtonState & _BTN) == 0 ? INPUT_RELEASED : INPUT_PRESSED;
}

INPUT_STATE PrevBtnStatus(_3DSButtons _BTN)
{
    return (m_Prev3DSButtonState & _BTN) == 0 ? INPUT_RELEASED : INPUT_PRESSED;
}

int KeyDown(_3DSButtons _BTN)
{
    // check if the key is being held down
    return CurBtnStatus(_BTN) == INPUT_PRESSED;
}

int KeyUp(_3DSButtons _BTN)
{
    // check if the key is not down
    return CurBtnStatus(_BTN) == INPUT_RELEASED;
}

int KeyPressed(_3DSButtons _BTN)
{
    // check if the key has just been pressed
    return CurBtnStatus(_BTN) == INPUT_PRESSED && PrevBtnStatus(_BTN) == INPUT_RELEASED;
}

int KeyReleased(_3DSButtons _BTN)
{
    // check if the key has just been released
    return CurBtnStatus(_BTN) == INPUT_RELEASED && PrevBtnStatus(_BTN) == INPUT_PRESSED;
}

circlePosition GetCirclepadState()
{
    return m_CirclepadPosition;
}
