
// #include "ExtendedConsole.h"
#include "MemoryManager.h"
#include "Types.h"

#include <assert.h>
#include <string.h>
#include <stdio.h>
#include <3ds.h>

#define MEMORY_BLOCK_FLAG 0x004d454d // "MEM"

typedef byte* MemoryAddress;

typedef enum
{
    BLOCK_USED  = 0x01,
    UNUSED_02   = 0x02,
    UNUSED_04   = 0x04,
    UNUSED_08   = 0x08,
    UNUSED_10   = 0x10,
    UNUSED_20   = 0x20,
    UNUSED_40   = 0x40,
    UNUSED_80   = 0x80
} BlockFlags;

typedef struct _block_header
{
    char ID[3];
    byte flags;
} BlockHeader;

typedef struct _memory_block
{
    BlockHeader block_header;
    uint32 block_size;
    MemoryAddress block_data; // last 4 bytes is a pointer to the top of the block
} MemoryBlock;

typedef struct _memory_pool
{
    uint32 total_size;
    uint32 remaining_size;
    MemoryBlock blocks[];
} MemoryPool;

    #if MEMORY_ENABLE_MAPPABLE
MemoryPool* pool_mappable;
    #endif 

    #if MEMORY_ENABLE_LINEAR
MemoryPool* pool_linear;
    #endif 
    
    #if MEMORY_ENABLE_VRAM
MemoryPool* pool_vram;
    #endif 

uint32 memory_pool_remaining(const MemoryPool* pool)
{
    return pool->remaining_size;
}

uint32 memory_pool_capacity(const MemoryPool* pool)
{
    return pool->total_size;
}

MemoryPool* GetPool(MemoryPoolType pool)
{
    switch(pool)
    {
    #if MEMORY_ENABLE_MAPPABLE
        case MEMORY_MAPPABLE:
            return pool_mappable;
    #endif 
    #if MEMORY_ENABLE_LINEAR
        case MEMORY_LINEAR:
            return pool_linear;
    #endif 
    #if MEMORY_ENABLE_VRAM
        case MEMORY_VRAM:
            return pool_vram;
    #endif 
    }
    return NULL;
}

void set_header_id(BlockHeader* header)
{
    header->ID[0] = 'M';
    header->ID[1] = 'E';
    header->ID[2] = 'M';
}

MemoryAddress* get_block_tail(const MemoryBlock* block)
{
    return (MemoryAddress*)(((byte*)&block->block_data) + block->block_size);
}

bool is_block_in_pool(const MemoryBlock* block, const MemoryPool* pool)
{
    return ((byte*)block) >= ((byte*)pool->blocks) &&
           (((byte*)block) < (((byte*)pool->blocks) + memory_pool_capacity(pool)));
}

void verify_block(const MemoryBlock* block)
{
    assert(block != NULL && "Invalid Block. Cannot be NULL.");
    assert(((MemoryBlock*)*get_block_tail(block)) == block && "Invalid block tail.  Did we overwright memory block bounds?");
    assert(((*(uint32*)&block->block_header) & 0x00FFFFFF) == MEMORY_BLOCK_FLAG && "Invalid block descriptor.  Did we overwright memory block bounds?");
}

void verify_pool_is_empty(const MemoryPool* pool)
{
    assert(memory_pool_remaining(pool) == memory_pool_capacity(pool) && "Pool not empty.  Do we have leaks?");
}

void cleanup_pool(MemoryPool* pool)
{
    MemoryBlock* block = pool->blocks;
    do
    {
        verify_block(block);
        if((block->block_header.flags & BLOCK_USED) != 0)
        {
            fprintf(stderr, "Leaked Memory 0x%lX for block 0x%lX\n", (uint32)block->block_data, (uint32)block);
        }
        block = (MemoryBlock*)(((byte*)get_block_tail(block)) + sizeof(MemoryAddress));

    } while (is_block_in_pool(block, pool));
    verify_pool_is_empty(pool);
}

void Memory_Init()
{
    #if MEMORY_ENABLE_MAPPABLE
    // init mappable memory
    uint32 size_mappable = mappableSpaceFree();
    pool_mappable = (MemoryPool*)mappableAlloc(size_mappable);
    pool_mappable->remaining_size = pool_mappable->total_size = size_mappable - sizeof(MemoryPool) - sizeof(MemoryBlock);
    
    MemoryBlock* block_mappable = pool_mappable->blocks;
    block_mappable->block_size = pool_mappable->remaining_size;
    block_mappable->block_header.flags = 0;
    MemoryAddress* pool_mappable_tail = get_block_tail(block_mappable);
    *pool_mappable_tail = (MemoryAddress)block_mappable;
    set_header_id(&block_mappable->block_header);
    #endif // MEMORY_ENABLE_MAPPABLE

    #if MEMORY_ENABLE_LINEAR
    // init linear memory
    uint32 size_linear = linearSpaceFree();
    pool_linear = (MemoryPool*)linearAlloc(size_linear);
    pool_linear->remaining_size = pool_linear->total_size = size_linear - sizeof(MemoryPool) - sizeof(MemoryBlock);

    MemoryBlock* block_linear = pool_linear->blocks;
    block_linear->block_size = pool_linear->remaining_size;
    block_linear->block_header.flags = 0;
    MemoryAddress* pool_linear_tail = get_block_tail(block_linear);
    *pool_linear_tail = (MemoryAddress)block_linear;
    set_header_id(&block_linear->block_header);
    #endif // MEMORY_ENABLE_LINEAR

    #if MEMORY_ENABLE_VRAM
    // init vram
    uint32 size_vram = vramSpaceFree();
    pool_vram = (MemoryPool*)vramAlloc(size_vram);
    pool_vram->remaining_size = pool_vram->total_size = size_vram - sizeof(MemoryPool) - sizeof(MemoryBlock);

    MemoryBlock* block_vram = pool_vram->blocks;
    block_vram->block_size = pool_vram->remaining_size;
    block_vram->block_header.flags = 0;
    MemoryAddress* pool_vram_tail = get_block_tail(block_vram);
    *pool_vram_tail = (MemoryAddress)block_vram;
    set_header_id(&block_vram->block_header);
    #endif // MEMORY_ENABLE_VRAM
}

void Memory_Shutdown()
{
    #if MEMORY_ENABLE_MAPPABLE
    cleanup_pool(pool_mappable);
    mappableFree(pool_mappable);
    #endif 

    #if MEMORY_ENABLE_LINEAR
    cleanup_pool(pool_linear);
    linearFree(pool_linear);
    #endif 

    #if MEMORY_ENABLE_VRAM
    cleanup_pool(pool_vram);
    vramFree(pool_vram);
    #endif 
}

byte* Memory_Pool_Allocate(MemoryPoolType pool, uint32 requested_size)
{
    MemoryPool* target_pool = GetPool(pool);

    // find open memory block
    MemoryBlock* cur_block = target_pool->blocks;
    verify_block(cur_block);
    while(((*(uint32*)&cur_block->block_header) & 0x00FFFFFF) == MEMORY_BLOCK_FLAG)
    {
        if((cur_block->block_header.flags & BLOCK_USED) == 0 && 
            cur_block->block_size >= requested_size + sizeof(MemoryBlock) + sizeof(MemoryBlock*))
        {
            // we have an open block - split it
            MemoryBlock* next_block = (MemoryBlock*)(((byte*)&cur_block->block_data) + requested_size + sizeof(MemoryBlock*));
            next_block->block_size = cur_block->block_size - requested_size - sizeof(MemoryBlock);
            MemoryAddress* next_block_tail = get_block_tail(next_block);
            *next_block_tail = (MemoryAddress)next_block;
            set_header_id(&next_block->block_header);
            next_block->block_header.flags = cur_block->block_header.flags;

            // set the current block data
            cur_block->block_size = requested_size;
            MemoryAddress* cur_block_tail = get_block_tail(cur_block);
            *cur_block_tail = (MemoryAddress)cur_block;
            cur_block->block_header.flags |= BLOCK_USED;

            // adjust remaining in the pool
            target_pool->remaining_size -= (requested_size + sizeof(MemoryBlock));

            return (byte*)&cur_block->block_data;
        }
        cur_block = (MemoryBlock*)(((byte*)&cur_block->block_data) + cur_block->block_size + sizeof(MemoryBlock*));
        if(!is_block_in_pool(cur_block, target_pool)) break;
        verify_block(cur_block);
    }

    return NULL;
}

byte* Memory_Pool_Reallocate(MemoryPoolType pool, byte* data, uint32 requested_size)
{
    if(data == NULL)
        return NULL;

    MemoryBlock* cur_block = (MemoryBlock*)(data - sizeof(MemoryBlock) + sizeof(MemoryAddress));
    verify_block(cur_block);
    if(cur_block->block_size == requested_size)
        return data;

    MemoryPool* target_pool = GetPool(pool);

    // handle shrinkage
    if(cur_block->block_size >= requested_size + sizeof(MemoryBlock))
    {
        MemoryAddress* cur_tail = get_block_tail(cur_block);
        target_pool->remaining_size += (cur_block->block_size - requested_size - sizeof(MemoryBlock));

        // close off the existing block
        cur_block->block_size = requested_size;
        MemoryAddress* new_tail = get_block_tail(cur_block);
        *new_tail = (MemoryAddress)cur_block;

        // setup the new block
        MemoryBlock* new_block = (MemoryBlock*)(((byte*)new_tail) + sizeof(MemoryAddress));
        set_header_id(&new_block->block_header);
        new_block->block_header.flags = 0;
        new_block->block_size = ((byte*)cur_tail) - ((byte*)&new_block->block_data);
        *cur_tail = (MemoryAddress)new_block;

        return data;
    }
    else
    {
        // can we merge in the next block
        MemoryBlock* next_block = (MemoryBlock*)(((byte*)&cur_block->block_data) + cur_block->block_size + sizeof(MemoryAddress));
        if(is_block_in_pool(next_block, target_pool) &&
            cur_block->block_size + next_block->block_size >= requested_size + sizeof(MemoryBlock))
        {
            verify_block(next_block);
            if((next_block->block_header.flags & BLOCK_USED) == 0)
            {
                // set the next block
                MemoryAddress* next_tail = get_block_tail(next_block);
                MemoryBlock* new_block = (MemoryBlock*)(((byte*)&cur_block->block_data) + requested_size + sizeof(MemoryAddress));
                new_block->block_size = (uint32)((byte*)next_tail - (byte*)&new_block->block_data);
                set_header_id(&new_block->block_header);
                new_block->block_header.flags = next_block->block_header.flags;
                *next_tail = (MemoryAddress)new_block;

                // set the updated block
                cur_block->block_size = requested_size;
                MemoryAddress* cur_tail = get_block_tail(cur_block);
                *cur_tail = (MemoryAddress)cur_block;

                target_pool->remaining_size -= sizeof(MemoryBlock);

                return data;
            }
        }

        // can we merge in the previous block
        MemoryAddress* prev_tail = (MemoryAddress*)(((byte*)cur_block) - sizeof(MemoryAddress));
        if(target_pool->blocks != cur_block) // handles first block
        {
            MemoryBlock* prev_block = (MemoryBlock*)(*prev_tail);
            if(is_block_in_pool(prev_block, target_pool) &&
                requested_size < prev_block->block_size + cur_block->block_size )
            {
                verify_block(prev_block);
                if((prev_block->block_header.flags & BLOCK_USED) == 0)
                {
                    // set the new block bounds
                    MemoryBlock* destination_block = (MemoryBlock*)(((byte*)cur_block) - (requested_size - cur_block->block_size));
                    destination_block->block_size = requested_size;
                    destination_block->block_header.flags = BLOCK_USED;
                    set_header_id(&destination_block->block_header);
                    MemoryAddress* destination_tail = get_block_tail(destination_block);
                    *destination_tail = (MemoryAddress)destination_block;
                    
                    prev_tail = (MemoryAddress*)(((byte*)destination_block) - sizeof(MemoryAddress));
                    prev_block->block_size = (byte*)prev_tail - (byte*)&prev_block->block_data;
                    *prev_tail = (MemoryAddress)prev_block;

                    // set the data to the new location
                    memmove(&destination_block->block_data, &cur_block->block_data, cur_block->block_size);
                    cur_block = destination_block;
                    target_pool->remaining_size -= (sizeof(MemoryBlock));

                    return (byte*)&cur_block->block_data;
                }
            }
        }

        // move data to the new location
        byte* new_allocation = Memory_Pool_Allocate(pool, requested_size);
        if(new_allocation)
        {
            memmove(new_allocation, &cur_block->block_data, cur_block->block_size);
            Memory_Pool_Free(pool, data);
            return new_allocation;
        }
    }
    return NULL;
}

byte* Memory_Pool_Callocate(MemoryPoolType pool, uint32 num, uint32 requested_size)
{
    byte* data = Memory_Pool_Allocate(pool, num * requested_size);
    if(data)
    {
        memset(data, 0, num * requested_size);
    }
    return data;
}

void Memory_Pool_Free(MemoryPoolType pool, byte* data)
{
    if(data == NULL)
        return;

    MemoryBlock* cur_block = (MemoryBlock*)(data - sizeof(MemoryBlock) + sizeof(MemoryAddress));
    verify_block(cur_block);
    if(cur_block)
    {
        MemoryPool* target_pool = GetPool(pool);
        target_pool->remaining_size += cur_block->block_size;

        // merge with next block if applicable
        MemoryBlock* next_block = (MemoryBlock*)(((byte*)&cur_block->block_data) + cur_block->block_size + sizeof(MemoryBlock*));
        if((MemoryAddress)next_block < (MemoryAddress)(((byte*)target_pool->blocks) + target_pool->total_size)) // handles last block
        {
            verify_block(next_block);
            if((next_block->block_header.flags & BLOCK_USED) == 0)
            {
                MemoryAddress* next_tail = get_block_tail(next_block);
                *next_tail = (MemoryAddress)cur_block;
                cur_block->block_size += next_block->block_size + sizeof(MemoryBlock);
                target_pool->remaining_size += sizeof(MemoryBlock);
            }
        }

        // merge with prev block if applicable
        MemoryAddress* prev_tail = (MemoryAddress*)(((byte*)cur_block) - sizeof(MemoryAddress));
        if(target_pool->blocks != cur_block) // handles first block
        {
            MemoryBlock* prev_block = (MemoryBlock*)(*prev_tail);
            verify_block(prev_block);
            if((prev_block->block_header.flags & BLOCK_USED) == 0)
            {
                MemoryAddress* cur_tail = get_block_tail(cur_block);
                *cur_tail = (MemoryAddress)prev_block;
                prev_block->block_size += cur_block->block_size + sizeof(MemoryBlock);
                cur_block = prev_block;
                target_pool->remaining_size += sizeof(MemoryBlock);
            }
        }
        cur_block->block_header.flags &= ~(BLOCK_USED);
    }
}

uint32 Memory_Pool_Remaining(MemoryPoolType pool)
{
    return memory_pool_remaining(GetPool(pool));
}

uint32 Memory_Pool_Capacity(MemoryPoolType pool)
{
    return memory_pool_capacity(GetPool(pool));
}

float Memory_Pool_Percent(MemoryPoolType pool)
{
    return ((float)Memory_Pool_Remaining(pool) / (float)Memory_Pool_Capacity(pool)) * 100.0f;
}

extern void PrintToScreen(gfxScreen_t TargetScreen, u16 X, u16 Y, char* szString, ...);
void Memory_Print()
{
    uint16 line = 0;
    #if MEMORY_ENABLE_MAPPABLE
    PrintToScreen(GFX_TOP, line++, 0, "mappableSpaceFree: 0x%08X - %f%%\n", Memory_Pool_Remaining(MEMORY_MAPPABLE), Memory_Pool_Percent(MEMORY_MAPPABLE));
    #endif 

    #if MEMORY_ENABLE_LINEAR
    PrintToScreen(GFX_TOP, line++, 0, "linearSpaceFree: 0x%08X - %f%%\n", Memory_Pool_Remaining(MEMORY_LINEAR), Memory_Pool_Percent(MEMORY_LINEAR));
    #endif 

    #if MEMORY_ENABLE_VRAM
    PrintToScreen(GFX_TOP, line++, 0, "vramSpaceFree: 0x%08X - %f%%\n", Memory_Pool_Remaining(MEMORY_VRAM), Memory_Pool_Percent(MEMORY_VRAM));
    #endif 

    // PrintToScreen(GFX_TOP, line++, 0, "heap: 0x%08X\n", 0, size_heap);
    line++;
    #if MEMORY_ENABLE_MAPPABLE
    PrintToScreen(GFX_TOP, line++, 0, "mappable address: 0x%08X - 0x%08X\n", (uint32)pool_mappable, (uint32)pool_mappable->blocks + Memory_Pool_Capacity(MEMORY_MAPPABLE) + sizeof(MemoryAddress));
    #endif 

    #if MEMORY_ENABLE_LINEAR
    PrintToScreen(GFX_TOP, line++, 0, "linear address: 0x%08X - 0x%08X\n", (uint32)pool_linear, (uint32)pool_linear->blocks + Memory_Pool_Capacity(MEMORY_LINEAR) + sizeof(MemoryAddress));
    #endif 

    #if MEMORY_ENABLE_VRAM
    PrintToScreen(GFX_TOP, line++, 0, "vram address: 0x%08X - 0x%08X\n", (uint32)pool_vram, (uint32)pool_vram->blocks + Memory_Pool_Capacity(MEMORY_VRAM) + sizeof(MemoryAddress));
    #endif 

    // PrintToScreen(GFX_TOP, line++, 0, "heap address: 0x%08X - 0x%08X\n", (uint32)pool_heap, (uint32)pool_heap + size_heap);
}