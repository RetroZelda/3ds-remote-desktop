#!/bin/sh
HOST='192.168.0.105'
PORT='5000'
NAME='3DSRemoteDesktopClient'

ncftp -P $PORT $HOST << EOF
binary
cd cia/
lcd ../bin/rom/
put $NAME.cia
cd ../3ds/$NAME
lcd ../client
put $NAME.3dsx
put $NAME.elf
put $NAME.smdh
quit
EOF
